<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\User;

class SendMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $tries = 3;
    protected $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

      // $id       = $this->contacto;
      // $contacto = Contacto::where('id', $id)->first();
      // $data     = [
      //     'user'    => $contacto->nombre . ', Email: ' . $contacto->email,
      //     'mensaje' => $contacto->mensaje,
      // ];
      // Mail::send('adminlte::contacto.mensaje', $data, function ($msg) {
      //     $msg->from('init.company.solutions@gmail.com', 'InitSolutions');
      //     $msg->to('franco170391@gmail.com')->subject('Urgente_InitSolutions');
      // });

      $user = $this->user;
      Log::info($user);
      Mail::send('email.confirmation_code',['user'=>$user],function($message) use($user){
        $message->from('acortlink@gmail.com','AcortLink');
        $message->to($user->email,$user->name)->subject('Por favor confirma tu correo');
      });

    }
}
