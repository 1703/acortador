<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Referral extends Model
{
    //
    use SoftDeletes;
    protected $table= "referral";
    protected $dates=['deleted_at'];
    protected $fillable=[
      'iduser',
      'codereferral',
    ];
}
