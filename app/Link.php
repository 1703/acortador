<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Link extends Model
{
    //
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $table="link";
    protected $fillable= [
      'code', 'url','user_id'
    ];
}
