<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    //
    use SoftDeletes;
    protected $table = 'users_email';
    protected $fillable =['email','users_id'];
    protected $dates=['deleted_at'];

    
}
