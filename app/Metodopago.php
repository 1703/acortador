<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Metodopago extends Model
{
    //
      use SoftDeletes;
      protected $table="metodopago";
      protected $dates=['deleted_at'];
      protected $fillable= [
        'metodopago','created_at','deleted_at'
      ];
}
