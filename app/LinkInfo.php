<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LinkInfo extends Model
{

  use SoftDeletes;
  protected $table="links_info";
  protected $fillable= [
      'idlink',
      'valor',
      'verificado',
      'check_referrall',
      'paystate',
      'uname',
      'periodo',
      'fecha',
       'remote_addr',
       'useragent',
       'hostname',
       'isp',
       'latitud',
       'longitud',
       'pais',
       'countrycode',
       'countryname',
       'continent',
       'regionname',
       'codigopostal',
       'codigoarea',
       'puerto',
       'zonahoraria',
       'anonimousproxy',
       'tipoproxy',
       'domainname',
       'city',
       'port',
       'timezone',
       'isproxy',
  ];

  protected $dates=['deleted_at'];


}
