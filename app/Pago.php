<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pago extends Model
{
    use SoftDeletes;
    protected $table = 'pago';
    protected $fillable =['fechapago_inicio','fechapago_fin','estado','monto','metodopago_id','email_id','user_id'];
    protected $dates=['deleted_at'];
    
}
