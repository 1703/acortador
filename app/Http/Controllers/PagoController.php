<?php

namespace App\Http\Controllers;

use App\Pago;
use App\Email;
use App\Metodopago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $earning_mes = "0";
        $earning_referrals = "0";
        $ganancia_total = $earning_mes + $earning_referrals;
        $metodopago = Metodopago::select('metodopago')->where('id',\Auth::user()->metodopago_id)->first();
        $email = Email::select('email')->where('users_id',\Auth::user()->id)->orderby('created_at','desc')->first();
        if (!empty($email)) {
          $email= $email->email;
        }else{
          $email = null;
        }
        $pagos=Pago::select('fechapago_inicio','fechapago_fin','estado','monto','users_email.email as email','metodopago.metodopago as metodopago')
        ->join('metodopago','metodopago.id','=','pago.metodopago_id')
        ->join('users_email','users_email.id','=','pago.email_id')
        ->where('user_id','=',Auth::user()->id)->orderBy('fechapago_fin','desc')->get();

        if (!empty($pagos)) {
          $fechapago = $pagos->first()->fechapago_fin;
        }else{
          $fechapago = date('Y-m-d');
        }
        return view('payments.index')->with([
          'earning_mes'=>$earning_mes,
          'earning_referrals'=>$earning_referrals,
          'ganancia_total' => $ganancia_total,
          'metodopago'=>$metodopago->metodopago,
          'email'=>$email,
          'pagos'=>$pagos,
          'fechapago'=>$fechapago
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function show(Pago $pago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function edit(Pago $pago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pago $pago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pago $pago)
    {
        //
    }
}
