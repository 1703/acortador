<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Pago;
use App\Http\Controllers\Controller;
use App\Email;
use App\Referral;
use App\Jobs\SendMailJob;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'terms' => ['required'],
            // 'password_confirmation' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // $data['confirmation_code']=str_random(120);
        $codereferral = (isset($data['codereferral'])) ? $data['codereferral'] : null ;
        $code = $this->rancode();

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'idcode'=> $code,
            'isadmin' => false,
            'metodopago_id' => 1,
            // 'confirmation_code'=>$data['confirmation_code'],
        ]);

        if (isset($codereferral) && !empty($codereferral)) {
          Referral::create([
            'iduser' => $user->id,
            'codereferral' => $codereferral,
          ]);
        }

        $email=Email::create([
          'email'=>$data['email'],
          'users_id'=>$user->id
        ]);

        Pago::create([
          'fechapago_inicio'=>Carbon::now()->setDate(date('Y'), date('m')+1, date('d'))->toDateString(),
          'fechapago_fin' => Carbon::now()->setDate(date('Y'), date('m')+2, 1)->toDateString(),
          'monto'=> '0.00000',
          'metodopago_id'=>$user->metodopago_id,
          'user_id'=>$user->id,
          'email_id'=>$email->id
        ]);


        // enviar mail para verificar (lo desactivo para que se loguen asi sin validar)
        // SendMailJob::dispatch($user)->delay(1);

        return $user;
    }




    public function register(Request $request)
    {

        if($this->validarCaptcha() != true){
          return back()->withErrors(['error'=>'Error captcha']);
        }
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        // return redirect('/login')->with('notification', 'Verifica tu mail para validar la cuenta.');

        $this->guard()->login($user);
        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }







    public function verify($code){
      $user= User::where('confirmation_code','=',$code)->first();
      if (!$user) {
        return view('errors.400');
      }
      $user->activo=true;
      $user->confirmation_code=null;
      $user->save();
      return redirect('/login')->with('notification', 'Validado correctamente.');
    }




}
