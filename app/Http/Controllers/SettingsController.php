<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pais;
use App\User;
use App\Pago;
use App\Email;
use App\Metodopago;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = \Auth::user();
      $pais = Pais::select('id','countryname','countrycode')->orderBy('countryname','asc')->pluck('countryname','id');
      $metodopago = Metodopago::select('metodopago','id')->pluck('metodopago','id');
      $email_payment = Email::select('email')->where('users_id',$user->id)->orderBy('created_at','desc')->first();
      return view('settings.edit')->with([
        'user'=>$user,
        'pais'=>$pais,
        'metodopago'=>$metodopago,
        'email_payment'=>$email_payment,
      ]);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request )
    {
        $validatedData = $request->validate([
          'name' => 'required|max:20',
          'lastname' => 'max:20',
          'ciudad' => 'max:20',
          'direccion' => 'max:20',
          'codepostal' => 'max:10',
          'metodopago_id' => 'max:10',
          'email_payment' => 'email',
        ]);

        $id = $request->names;
        $email = $request->email_payment;
        User::where('id',$id)->update([
          "name" => $request->name,
          "lastname" => $request->lastname,
          "ciudad" => $request->ciudad,
          "direccion" => $request->direccion,
          "codepostal" => $request->codepostal,
          "pais_id" => $request->pais_id,
          "metodopago_id" => $request->metodopago_id,
        ]);
        $mail = Email::where('email',$email)->first();
        if(!$mail){
          $e = Email::create([
            'email'=> $email,
            'users_id' => $id,
          ]);
          Pago::where('user_id',$id)->update([
            'email_id'=>$e->id,
          ]);
        }
        return back();
    }


}
