<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // vistas del mes y ganancias del mes (cantidad de clicks)
        $viewsthismonth=DB::table('links_info as li')
          ->select(DB::raw('count(li.verificado) as clicksthismonth, round(sum(li.valor),4) as earning'))
          ->join('link as l','l.id','=','li.idlink')
          ->where('l.user_id','=',Auth::user()->id)
          // ->where('li.verificado','=',false)
          // ->where('li.isproxy','=',false)
          ->where('li.periodo','=',date('Y-m'))
          ->get();
        // dd($viewsthismonth);

        // allearning, todas las ganancias
        $allearning=DB::table('links_info as li')
          ->select(DB::raw('count(li.verificado) as allclicks, round(sum(li.valor),4) as earning'))
          ->join('link as l','l.id','=','li.idlink')
          ->where('l.user_id','=',Auth::user()->id)
          ->get();

        return view('home')->with([
          'viewthismonth'=> $viewsthismonth[0],
          'allearning' => $allearning[0],
        ]);
    }// end index



    public function viewsthismonth(){
        // vistas ordenadas por dia del mes
        $viewsthismonth = $this->getClickEarning('clicksthismonth');

        $cantDiasMes = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $dias[] =[];
        $clicksthismonth[]=[];
        for ($i=0; $i <= $cantDiasMes ; $i++) {
          $dias[$i] = $i;
          if(isset($viewsthismonth[$i])){
            $clicksthismonth[$i] = $viewsthismonth[$i];
          }else{
            $clicksthismonth[$i] = 0;
          }
        }

      return response()->json(['data'=>[
          'dias' => $dias,
          'clicksthismonth' =>$clicksthismonth,
        ]]);
    }// end function




    public function earningthismonth(){
      $viewsthismonth = $this->getClickEarning('earning');

      $cantDiasMes = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
      $dias[] =[];
      $earning[]=[];
      for ($i=0; $i <= $cantDiasMes ; $i++) {
        $dias[$i] = $i;
        if(isset($viewsthismonth[$i])){
          $earning[$i] = $viewsthismonth[$i];
        }else{
          $earning[$i] = 0;
        }
      }

      return response()->json(['data'=>[
        'dias' => $dias,
        'earning' =>$earning,
      ]]);
    }// end function



    public function getClickEarning($texto){
      // date format msyql %e para numeros sin ceros
      $data=DB::table('links_info as li')
      ->select(DB::raw('count(li.verificado) as clicksthismonth, round(sum(li.valor),4) as earning , date_format(li.created_at,"%e") as mesdia'))
      ->join('link as l','l.id','=','li.idlink')
      ->where('l.user_id','=',Auth::user()->id)
      // ->where('li.verificado','=',false)
      // ->where('li.isproxy','=',false)
      ->where('li.periodo','=', date('Y-m'))
      ->groupby('mesdia')
      ->pluck($texto,'mesdia');
      return $data;
    }
}
