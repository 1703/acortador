<?php

namespace App\Http\Controllers;

use App\Metodopago;
use Illuminate\Http\Request;

class MetodopagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Metodopago  $metodopago
     * @return \Illuminate\Http\Response
     */
    public function show(Metodopago $metodopago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Metodopago  $metodopago
     * @return \Illuminate\Http\Response
     */
    public function edit(Metodopago $metodopago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Metodopago  $metodopago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Metodopago $metodopago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Metodopago  $metodopago
     * @return \Illuminate\Http\Response
     */
    public function destroy(Metodopago $metodopago)
    {
        //
    }
}
