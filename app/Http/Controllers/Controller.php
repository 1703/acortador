<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function urls(){
      return $urls = array(
        'http://deloplen.com/afu.php?zoneid=2711931',
        'http://deloplen.com/afu.php?zoneid=2708974',
        'http://deloplen.com/afu.php?zoneid=2711934',
      );
    }

    public function ramdomCode(){
      $urls = $this->urls();
      $url = $urls[rand(0,sizeof($urls)-1)];
      return $url;
    }

    public function ramdomCode2(){
      $urls = $this->urls();
      $url = $urls[rand(0,sizeof($urls)-1)];
      $code = Link::select('code')->where('url',$url)->first();
      if(!empty($code)){
        $code = $code->code;
        return $code;
      }
      $code = $this->rancode();
      return $code;
    }

    // devuelve el precio de cada link
    public  function f_rand(){
      // de 0,80 a 1,50 USD
        $min=0.00070;
        $max=0.0015;
        $mul=10000;
        if ($min>$max) return false;
        return mt_rand($min*$mul,$max*$mul)/$mul;
    }

    // devuelve un strin de 9 caracteres alfanumericos
    public  function rancode(){
      $an = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      $su = strlen($an) - 1;
      return $abc=
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1);
    }

    

    public function responseJson($status,$mensaje){
      return response()->json(['data'=>[
        'status'=>$status,
        'msj' =>$mensaje
      ]]);
    }


    public function validarCaptcha(){
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          // Creamos el enlace para solicitar la verificación con la API de Google.
          $params = array();  // Array donde almacenar los parámetros de la petición
          $params['secret'] = "6LcRPSsUAAAAAJr2In0gJcDbQIpCO33GCZ_yrx7S"; // Clave privada
          if (!empty($_POST) && isset($_POST['g-recaptcha-response'])) {
          $params['response'] = urlencode($_POST['g-recaptcha-response']);
          }
          $params['remoteip'] = $_SERVER['REMOTE_ADDR'];

          // Generar una cadena de consulta codificada estilo URL
          $params_string = http_build_query($params);
          // Creamos la URL para la petición
          $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $params_string;

          // Inicia sesión cURL
          $curl = curl_init();
          // Establece las opciones para la transferencia cURL
          curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $requestURL,
          ));

          // Enviamos la solicitud y obtenemos la respuesta en formato json
          $response = curl_exec($curl);
          // Cerramos la solicitud para liberar recursos
          curl_close($curl);
          // Devuelve la respuesta en formato JSON
          $response= json_decode($response,true);
          if ($response['success'] ==true) {
            return true;
          }else {
            // echo "<br> no se reconoce la pagina<br>";
            return false;
          }
        }
        return false;
    }// end validarCaptcha

}
