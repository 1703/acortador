<?php

namespace App\Http\Controllers;

use App\Link;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $links= Link::all()->paginate(2);
        $links=Link::select('id','code','url',\DB::raw('date(created_at) as created_at'))
                ->where('user_id','=',\Auth::user()->id)
                ->orderBy('id','desc')
                ->paginate(100);
        return view('links.index')->with(['links'=>$links]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function api(Request $data){

        if (isset($data->token)) {
            if (isset($data->uri)) {
                if (strlen($data->token)==9){
                    $user=User::where('idcode','=',$data->token)->get();
                    // dd($user[0]->id);
                    if (sizeof($user)==1) {
                        $url = filter_var($data->uri, FILTER_SANITIZE_URL);
                        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
                            $links=Link::withTrashed()->where('url','=',$url)->where('user_id','=',$user[0]->id)->get();
                            if (sizeof($links)==1) {
                                $links[0]->restore();
                                return $this->responseJson('ok',url('/',$links[0]->code));
                            }else{
                                //BEGIN crear un link
                                $code = $this->rancode();
                                $linkCode=Link::select(DB::raw('count(*) as cantidad'))->where('code','=',$code)->pluck('cantidad');
                                $cantidad=$linkCode[0];
                                while (!$cantidad==0) {
                                    $code= $this->rancode();
                                    $linkCode=Link::select(DB::raw('count(*) as cantidad'))->where('code','=',$code)->pluck('cantidad');
                                    if ($linkCode[0]==0) {
                                      $cantidad=0;
                                    }else{
                                      $cantidad=1;
                                    }
                                }
                                $link=new Link;
                                $link->code=$code;
                                $link->url=$url;
                                $link->user_id=$user[0]->id;
                                $link->save();
                                return $this->responseJson('ok',url('/',$code));
                                //END crear un link
                            }
                        }else{
                            return $this->responseJson('error','url no valida');
                        }
                    }else{
                        return $this->responseJson('error','token no valido');
                    }
                }else{
                    return responseJson('error','token no valido');
                }
            }else{
                return $this->responseJson('error','uri no existe');
            }
        }else{
            return $this->responseJson('error','error params');
        }

     }// end function


    public function create()
    {
        //
        return view('links.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idUser = \Auth::user()->id;
        $url = filter_var($request->url, FILTER_SANITIZE_URL);
        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            $links=Link::withTrashed()->where('url','=',$url)->where('user_id','=',$idUser)->pluck('code');
            if (sizeof($links)!=0) {
              //si existe en la db mostrar el code de la url
              foreach ($links as $k=> $v) {
                $valor=$v;
              }
              return back()->with(['status'=>'Url creada con exito.']);
              // return response()->json(["code"=>url('/',$valor)]);
            }else{
              //si no existe la url en la db crear code.
              $code = $this->rancode();
              $linkCode=Link::select(DB::raw('count(*) as cantidad'))->where('code','=',$code)->pluck('cantidad');
              $cantidad=$linkCode[0];
              while (!$cantidad==0) {
                $code= $this->rancode();
                $linkCode=Link::select(DB::raw('count(*) as cantidad'))->where('code','=',$code)->pluck('cantidad');
                if ($linkCode[0]==0) {
                  $cantidad=0;
                }else{
                  $cantidad=1;
                }
              }
              $link=new Link;
              $link->code=$code;
              $link->url=$url;
              $link->user_id=$idUser;
              $link->save();
              return back()->with(['status'=>'Url creada con exito.']);

              // return response()->json(["code"=>url('/',$code)]);
            }
        } else {
          // url no valida
          return back()->with(['error'=>'Url no valida']);

            // return response()->json(["code"=>$url." no valida"]);
        }

    }


     public function stor(Request $request)
     {
         if ($request->ajax()) {
           if ( Auth::check()) {
             $user_id = Auth::user()->id;
           }else{
             $user_id=1;
           }
           $url = filter_var($request->url, FILTER_SANITIZE_URL);
           if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
               $links=Link::withTrashed()->where('url','=',$url)->where('user_id','=',$user_id)->pluck('code');
               if (sizeof($links)!=0) {
                 //si existe en la db mostrar el code de la url
                 foreach ($links as $k=> $v) {
                   $valor=$v;
                 }
                 return back()->with(['status'=>'Url creada con exito.']);
                 // return response()->json(["code"=>url('/',$valor)]);
               }else{
                 //si no existe la url en la db crear code.
                 $code = $this->rancode();
                 $linkCode=Link::select(DB::raw('count(*) as cantidad'))->where('code','=',$code)->pluck('cantidad');
                 $cantidad=$linkCode[0];
                 while (!$cantidad==0) {
                   $code= $this->rancode();
                   $linkCode=Link::select(DB::raw('count(*) as cantidad'))->where('code','=',$code)->pluck('cantidad');
                   if ($linkCode[0]==0) {
                     $cantidad=0;
                   }else{
                     $cantidad=1;
                   }
                 }
                 $link=new Link;
                 $link->code=$code;
                 $link->url=$url;
                 $link->user_id= $user_id;
                 $link->save();
                 // return back()->with(['status'=>'Url creada con exito.']);
                 return response()->json(['code'=> url('/')."/".$link->code]);

                 // return response()->json(["code"=>url('/',$code)]);
               }
           } else {
             // url no valida, envio una url no valida
               return response()->json(["code"=>url('/')."/".$this->rancode()]);
           }
         }else{
           //no ajax
           return view('errors.404');

         }

     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function edit($link)
    {
      if (ctype_digit($link)) {
        $link = Link::select('id','code','url')->where('id',$link)->where('user_id',Auth::user()->id)->first();
        if (!empty($link)) {
          return view('links.edit')->with(['link'=>$link]);
        }
        return view('errors.404');
      }else{
        return view('errors.404');
      }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $link)
    {
        if (ctype_digit($link)) {
          $url = $request->url;
          $url = filter_var($url, FILTER_SANITIZE_URL);
          if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            Link::where('id',$link)->where('user_id',Auth::user()->id)->update([
              'url'=> $request->url,
            ]);
          }
          return back();
        }else{
          return view('errors.404');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function destroy($link)
    {
      if (ctype_digit($link)) {
        $lin = Link::where('id',$link)->where('user_id',Auth::user()->id)->first();
        if(!empty($lin)){
          $lin->delete();
          return back();
        }else{
          return view('errors.404');
        }
      }else {
        return view('errors.404');
      }

    }
}
