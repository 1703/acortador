<?php

namespace App\Http\Controllers;
use App\User;
use App\Referral;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class ReferralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $referrals=DB::table('referral')->select('users.name as name','users.id as id')
        ->join('users','users.id','=','referral.iduser')
        ->where('referral.codereferral',Auth::user()->idcode)
        ->get();

        $earning[]=0;
        $myearning[]=0;
        if (!empty($referrals)) {
          //para cada referido obtener el 20% de sus ganancias por mes
          foreach ($referrals as $key => $u) {
            $earning_m=DB::table('links_info as li')
              ->select(DB::raw('sum(li.valor) * 20 / 100 as myearning, sum(li.valor) as earning'))
              ->join('link as l','l.id','=','li.idlink')
              ->where('l.user_id','=',$u->id)
              ->where('li.verificado','=',true)
              ->where('li.isproxy','=',false)
              ->where('paystate','=',false)
              ->where('li.periodo','=',date('Y-m'))
              ->get();
              if ($earning_m[0]->myearning==0 || $earning_m[0]->earning==0) {
                $earning[]=0;
                $myearning[]=0;
              }else {
                $earning[]=$earning_m[0]->earning;
                $myearning[]= $earning_m[0]->myearning;
              }
          }
        }else {
          // $earning[]=0;
          // $myearning[]=0;
          $referrals=null;
        }

        return view('referral.index')->with([
          'referrals'=>$referrals,
          'earning' => $earning,
          'myearning' => $myearning,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ref($codereferral)
    {
        // consulto la longitud de strlen
        $longitud = strlen($this->rancode());
        if(strlen($codereferral) ==  $longitud){
          $user = User::select('name')->where('idcode',$codereferral)->first();
          if(!empty($user)){
            return view('auth.register')->with('codereferral',$codereferral);
          }
          return view('auth.register');
        }else {
          return view('errors.400');// enviar a falso register
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function show(Referral $referral)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function edit(Referral $referral)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Referral $referral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function destroy(Referral $referral)
    {
        //
    }
}
