<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Link;
use App\LinkInfo;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code)
    {
      
      if(strlen($code) == strlen($this->rancode())){
        return view('search.index')->with(['code'=>$code]);
      }
      return redirect()->to(url('/').'/'.$this->ramdomCode2());
    }



    public function captchaForm(){
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          // Creamos el enlace para solicitar la verificación con la API de Google.
          $params = array();  // Array donde almacenar los parámetros de la petición
          $params['secret'] = "6LcRPSsUAAAAAJr2In0gJcDbQIpCO33GCZ_yrx7S"; // Clave privada
          if (!empty($_POST) && isset($_POST['g-recaptcha-response'])) {
          $params['response'] = urlencode($_POST['g-recaptcha-response']);
          }
          $params['remoteip'] = $_SERVER['REMOTE_ADDR'];

          // Generar una cadena de consulta codificada estilo URL
          $params_string = http_build_query($params);
          // Creamos la URL para la petición
          $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $params_string;

          // Inicia sesión cURL
          $curl = curl_init();
          // Establece las opciones para la transferencia cURL
          curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $requestURL,
          ));

          // Enviamos la solicitud y obtenemos la respuesta en formato json
          $response = curl_exec($curl);
          // Cerramos la solicitud para liberar recursos
          curl_close($curl);
          // Devuelve la respuesta en formato JSON
          $response= json_decode($response,true);
          if ($response['success'] ==true) {
            return $this->valida($_POST['code']);
          }else {
            // echo "<br> no se reconoce la pagina<br>";
            return view('errors.404');
          }
        }else {
          // echo "<br> no es post<br>";
          return view('errors.404');
      }
    }// end function


    public function valida($code){
      // $code= $request->code;
      if (isset($code) && !empty($code)) {
        session_start();
        // verifico que si la ip es una proxy y lo guardo en una variable session
        // $ip='142.93.9.124';
        // $ip=$faker->ipv4;
        $ip=$_SERVER['REMOTE_ADDR'];
        if (Cache::has($ip)) {
          $datos=Cache::get($ip);
          $_SESSION['PROXY']= $datos['isproxy'];
        }else{
          //http://www.shroomery.org/ythan/proxycheck.php not limit QUERY
          $url="http://www.shroomery.org/ythan/proxycheck.php?ip=".$ip;
          $proxy= @file_get_contents($url);
          $valor=Cache::remember($ip, 3,function() use ($proxy){
            return $proxy;
          });
          $_SESSION['PROXY']= $proxy;
        }
        // \Log::info('proxy = '.$_SESSION['PROXY'].', ip: '.$ip);
        return view('search.ok')->with(['code'=>$code]);
      }
      return view('errors.404');
    }



    public function ok(Request $request){
        session_start();

        $code = $request->code;
        $r=Link::select('id','url')->where('code','=',$code)->get();
        if (!sizeof($r)==0) {
          //proxy    173.208.82.166
          // $faker = Faker::create();
          $ip=$_SERVER['REMOTE_ADDR'];
          // $ip='142.93.9.124';
          // $ip=$faker->ipv4;
          // http://www.ipinfodb.com/ip_location_api.php  LIMIT 2 QUERY PER SECOND
          if (Cache::has('api'.$ip)) {
            $details=Cache::get('api'.$ip);

          }else{
            $api= "449b8feca54b8a0b536373255f66e9e78ed4aed481a784842cc76950e16e66ae";
            @$details=json_decode(file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key='.$api.'&ip='.$ip.'&format=json'));
            Cache::remember('api'.$ip,30,function() use ($details){
              return $details;
            });
          }
          // { // esto es de api.ipinfodb.com
          //   "statusCode" : "OK",
          //   "statusMessage" : "",
          //   "ipAddress" : "142.93.9.124",
          //   "countryCode" : "US",
          //   "countryName" : "United States",
          //   "regionName" : "New Jersey",
          //   "cityName" : "North Bergen",
          //   "zipCode" : "07047",
          //   "latitude" : "40.8043",
          //   "longitude" : "-74.0121",
          //   "timeZone" : "-04:00"
          // }

          // http://api.ipinfodb.com/v3/ip-city/?key=449b8feca54b8a0b536373255f66e9e78ed4aed481a784842cc76950e16e66ae&ip=   &format=json
          // @$details=unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
          // @$details=unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip)); //120 query per min
          // {
          //   "geoplugin_request" => "142.93.9.124"
          //   "geoplugin_status" => 200
          //   "geoplugin_delay" => "1ms"
          //   "geoplugin_credit" => "Some of the returned data includes GeoLite data created by MaxMind, available from <a href=\'http://www.maxmind.com\'>http://www.maxmind.com</a>."
          //   "geoplugin_city" => "North Bergen"
          //   "geoplugin_region" => "New Jersey"
          //   "geoplugin_regionCode" => "NJ"
          //   "geoplugin_regionName" => "New Jersey"
          //   "geoplugin_areaCode" => ""
          //   "geoplugin_dmaCode" => "501"
          //   "geoplugin_countryCode" => "US"
          //   "geoplugin_countryName" => "United States"
          //   "geoplugin_inEU" => 0
          //   "geoplugin_euVATrate" => false
          //   "geoplugin_continentCode" => "NA"
          //   "geoplugin_continentName" => "North America"
          //   "geoplugin_latitude" => "40.7939"
          //   "geoplugin_longitude" => "-74.0258"
          //   "geoplugin_locationAccuracyRadius" => "1000"
          //   "geoplugin_timezone" => "America/New_York"
          //   "geoplugin_currencyCode" => "USD"
          //   "geoplugin_currencySymbol" => "&#36;"
          //   "geoplugin_currencySymbol_UTF8" => "$"
          //   "geoplugin_currencyConverter" => "1"
          // }
          
          // cantidad por code
          // $link=LinkInfo::select('*')
          //   ->where('created_at','like',date('Y-m-d').'%')
          //   ->where('idlink','=',$r[0]->id)
          //   ->where('remote_addr','=',$ip)
          //   ->count();
          // cantidad por code y controlar accesos por proxys

          $linkinfo=new LinkInfo;
          if ($_SESSION['PROXY']==='Y') {
            $linkinfo->isproxy='si';
          }elseif ($_SESSION['PROXY']==='N') {
            $linkinfo->isproxy='no';
          }elseif($_SESSION['PROXY']==='E'){
            //CONTROLAR EL ERROR E
          }
          unset($_SESSION['PROXY']);

          // if ($link>=0 && $link<2) { //2 por ip
          //   $linkinfo->verificado=true;
          // }else {
          // }
          // $linkinfo->verificado=null;
          $linkinfo->useragent=$_SERVER['HTTP_USER_AGENT'];
          $linkinfo->remote_addr=$_SERVER['REMOTE_ADDR'];
          $linkinfo->hostname=gethostbyaddr($ip);
          if (!empty($details) && isset($details)) {
            if ($details->statusCode=="OK") {
                $linkinfo->isp = (isset($details->org)) ? $details->org : 0 ;
                $linkinfo->regionname   = (isset($details->regionName)) ? $details->regionName : 0 ; //tucuman
                $linkinfo->city         = (isset($details->cityName)) ? $details->cityName : 0 ;  //san miguel de tucuman
                $linkinfo->countryCode  = (isset($details->countryCode)) ? $details->countryCode : 0 ;
                $linkinfo->countryname  = (isset($details->countryName)) ? $details->countryName : 0 ;//pais
                $linkinfo->codigopostal = (isset($details->zipCode)) ? $details->zipCode : 0 ;
                $linkinfo->latitud      = (isset($details->latitude)) ? $details->latitude : 0 ;
                $linkinfo->longitud     = (isset($details->longitude)) ? $details->longitude : 0 ;
                $linkinfo->timezone     = (isset($details->timeZone)) ? $details->timeZone : 0 ;
            }
          }
          $linkinfo->port=$_SERVER['REMOTE_PORT'];
          $linkinfo->valor=$this->f_rand();// de 0,80 a 1,50 USD
          $linkinfo->idlink=$r[0]->id;
          $linkinfo->periodo=  date('Y-m');
          $linkinfo->fecha =  date('Y-m-d');
          $linkinfo->save();
          session_unset();
          session_destroy();
          return redirect()->to($r[0]->url);
          // return response()->json(['li'=>$r[0]->url]);
        }else{
          return redirect()->to($this->ramdomCode());
          // return view('errors.404');
          // return response()->json(['li'=>url('/errors/404')]);
        }
    }


    // public function ok0(){
    //   // preguntar si la ip ya existe, recibir solo 3 ip iguales luego no contarlas.
    //   session_start();
    //   if (!empty($_SESSION['code']) && isset($_SESSION['code'])) {
    //     if (strlen($_SESSION['code'])==9) {
    //       $r=Link::select('id','url')->where('code','=',$_SESSION['code'])->get();
    //       if (!sizeof($r)==0) {
    //         //proxy    173.208.82.166
    //         // $faker = Faker::create();
    //         // $ip=$_SERVER['REMOTE_ADDR'];
    //         $ip='186.109.197.14';
    //         // $ip=$faker->ipv4;
    //         // http://www.ipinfodb.com/ip_location_api.php  LIMIT 2 QUERY PER SECOND
    //         if (Cache::has('api'.$ip)) {
    //           $details=Cache::get('api'.$ip);
    //         }else{
    //           $api= "449b8feca54b8a0b536373255f66e9e78ed4aed481a784842cc76950e16e66ae";
    //           @$details=json_decode(file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key='.$api.'&ip='.$ip.'&format=json'));
    //           Cache::remember('api'.$ip,30,function() use ($details){
    //             return $details;
    //           });
    //         }

    //         // http://api.ipinfodb.com/v3/ip-city/?key=449b8feca54b8a0b536373255f66e9e78ed4aed481a784842cc76950e16e66ae&ip=   &format=json
    //         // dd($details);
    //         // @$details=unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
    //         // @$details=unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip)); //120 query per min
    //         // cantidad por code
    //         $link=LinkInfo::select('*')
    //           ->where('fecha','like',date('Y-m-d'))
    //           ->where('idlink','=',$r[0]->id)
    //           ->where('remote_addr','=',$ip)
    //           ->count();
    //         // cantidad por code y controlar accesos por proxys
    //         $linkinfo=new LinkInfo;
    //         if ($_SESSION['PROXY']=='Y') {
    //           $linkinfo->isproxy=true;
    //         }elseif ($_SESSION['PROXY']=='N') {
    //           $linkinfo->isproxy=false;
    //         }elseif($_SESSION['PROXY']=='E'){
    //           //CONTROLAR EL ERROR E
    //         }
    //         unset($_SESSION['PROXY']);

    //         if ($link>=0 && $link<2) { //2 por ip
    //           $linkinfo->verificado=true;
    //         }else {
    //           $linkinfo->verificado=false;
    //         }
    //         $linkinfo->useragent=$_SERVER['HTTP_USER_AGENT'];
    //         $linkinfo->remote_addr=$_SERVER['REMOTE_ADDR'];
    //         $linkinfo->hostname=gethostbyaddr($ip);
    //         $linkinfo->remote_addr=$ip;
    //         if (!empty($details)) {
    //           if ($details->statusCode=="OK") {
    //               // $linkinfo->isp          =$details->org;
    //               $linkinfo->regionname   =$details->regionName; //tucuman
    //               $linkinfo->city         =$details->cityName;  //san miguel de tucuman
    //               $linkinfo->countryCode  =$details->countryCode;
    //               $linkinfo->countryname  =$details->countryName;//pais
    //               // $linkinfo->continent    =$details->geoplugin_continentCode;
    //               $linkinfo->codigopostal =$details->zipCode;
    //               $linkinfo->latitud      =$details->latitude;
    //               $linkinfo->longitud     =$details->longitude;
    //               $linkinfo->timezone     =$details->timeZone;
    //           }
    //         }
    //         $linkinfo->port=$_SERVER['REMOTE_PORT'];
    //         // $linkinfo->valor=$this->f_rand(0.00070,0.0015);// de 0,80 a 1,50 USD
    //         $linkinfo->idlink=$r[0]->id;
    //         $linkinfo->save();
    //         session_unset();
    //         session_destroy();
    //         // return redirect()->to($r[0]->url);
    //         return response()->json(['li'=>$r[0]->url]);
    //       }else{
    //         session_unset();
    //         session_destroy();
    //         // return view('errors.400');
    //         return response()->json(['li'=>url('/errors/404')]);
    //       }
    //     }else return view('errors.404');
    //   }else return view('errors.404');//session code
    //   // return "link no existe";
    // }


}// end class
