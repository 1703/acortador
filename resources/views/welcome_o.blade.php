
<!DOCTYPE html>
<html lang="en">
@include('layouts.partials.htmlheader')
{{-- @include('layouts.publicidad.happytag_antiadblock') --}}

<body data-spy="scroll"  class="skin-green">
<div id="navigation" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=""><b>@include('layouts.partials.title')</b></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">

            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}"><b>Login</b></a></li>
                    <li><a href="{{ url('/register') }}"><b>{{ trans('adminlte_lang::message.register') }}</b></a></li>
                @else
                    <li><a href="/home"><b>{{ Auth::user()->name }}</b></a></li>
                @endif
            </ul>
        </div>
    </div>
</div>




{{-- <section id="home" name="home"></section> --}}
<div id="home">
    <div class="container ">
        {{-- <div class="row centered"> --}}
            <div class="col-lg-12 text-center">
                <h1 >
                    <b>THE BEST URL SHORTENER</b>
                </h1>
                <h3 >
                    Sharing links on the internet. Get paid for every person who visits your URLs
                </h3>
            </div>
              <div class="col-sm-12 col-md-10 col-md-offset-1 foo pe " id="short"  >
                  <form action="{{ url('link') }}" id="myform">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                  {{-- {!!Form::hidden('_token',csrf_token())!!} --}}
                      <div class="input-group input-group-lg"  >
                          <span class="input-group-btn" >
                              <button class="btn btn-warning btn-flat " type="button" id="copy">
                                <i class='fa fa-copy'></i>
                              </button>
                          </span>
                          <input name="url" id="dat" type="text" class="form-control" placeholder="http://www.yourlink..." value="">
                          <span class="input-group-btn">
                              <button class="btn btn-primary btn-flat" id="btn-flat" type="submit">GO!</button>
                          </span>
                      </div>
                  </form>
              </div>
        {{-- </div> --}}
    </div> <!--/ .container -->
</div>







{{-- <section id="showcase" name="showcase"></section> --}}
<div id="showcase" >
    <div class="container">
        <div class="row centered">
            <h1 class="centered" >
              {{-- <b>Use our shortened service</b> --}}
              <b>USE OUR SHORTENED SERVICE</b>
            </h1>
            <br>
            <h4>Sign up using your email</h4>
            <br>
            <div class="col-lg-8 col-lg-offset-2  singup">
                <div class="btn btn-danger btn-lg"  >
                    <a href="{{url('/register')}}">SIGN UP NOW!</a>
                </div>
            </div>
        </div>
    </div><!-- /container -->
</div>

{{-- <section id="about" name="about"></section> --}}
<div id="about" >
    <div class="container">
        <div class="row ">
            {{-- <h1 class="text-center" ><b>ABOUT ACORTLINK</b></h1> --}}
            <div class="col-lg-6 centered">
                <h2>What is the @include('layouts.partials.title') </h2>
                <p>
                    @include('layouts.partials.title') is a great tool for any user looking to earn money by shortening their URLs.
                </p>
            </div>
            <div class="col-lg-6 centered">
              <h2>Statistics </h2>
              <p>
                You can see at all time the statistics of your links, perform a daily control of each link visit and see the money generated.
              </p>
            </div>
{{--
            <div class="col-lg-6 centered">
                <img class="centered" src="{{ asset('/img/mobile.png') }}" alt="">
            </div> --}}
        </div>
    </div><!--/ .container -->
</div>


{{-- <section id="desc" name="desc"></section> --}}
<div id="desc">
    <div class="container">
        <div class="row centered">
            <div class="col-lg-4  col-sm-12 col-md-12 user">
                <span id="ico" class="fa fa-user"></span>
                <p><span class="h3">Create an Account</span></p>
            </div>
            <div  class="col-lg-4 col-sm-12 col-md-12">
                <span id="ico" class="fa fa-chain"></span>
                <p><span class="h3">Shorte Your Link</span></p>
                <p><span class="h5">Add URLs and shorten your links to monetize.</span></p>

            </div>
            <div class="col-lg-4 col-sm-12 col-md-12">
                <span id="ico" class="fa fa-money"></span>
                <p><span class="h3">Earn Money</span></p>
                <p><span class="h5">Get paid for every real person who visit your links.</span></p>
            </div>
        </div>
    </div> <!-- / .container  -->
</div>


{{-- <section id="faq" name="faq"></section> --}}
<div id="faq">
    <div class="container">
        <div class="row">
            <h1 class="text-center"><b>FREQUENTLY ASKED QUESTIONS</b></h1>
            <br>
            <br>
            <div class="col-lg-6 centered">
                <img class="centered" src="{{ asset('/img/mobile.png') }}" alt="">
            </div>

            <div class="col-lg-6">
                <div class="accordion ac" id="accordion" aria-multiselectable="true">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                1. What is the minimum payment at @include('layouts.partials.title') ?
                            </a>
                        </div>
                        <div id="collapseOne" class="accordion-body collapse in" aria-labelledby="headingOne">
                            <div class="accordion-inner">
                              <br>
                                <p>@include('layouts.partials.title') will make the payment when users have earned a sum of $ @include('layouts.partials.usd'). Once users have accrued $ @include('layouts.partials.usd'), they will receive the payment. </p>
                            </div><!-- /accordion-inner -->
                        </div><!-- /collapse -->
                    </div><!-- /accordion-group -->
                    <br>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                2. When does @include('layouts.partials.title') pay ?
                            </a>
                        </div>
                        <div id="collapseTwo" class="accordion-body collapse in" aria-labelledby="headingTwo">
                            <div class="accordion-inner">
                                <p><br>
                                    Here’s a reason to celebrate! @include('layouts.partials.title') pays users every fortnight. Payments are made on the 1st and 16th day of the month. See below for an example:
                                    <br>
                                    - All money generated between the 1st and the 15th of January will be credited to the user’s account on the 16th of January.
                                    <br>
                                    - All money generated between the 16th and the 31st of January will be credited to the user’s account on the 1st of February.
                                    <br>
                                </p>
                            </div><!-- /accordion-inner -->
                        </div><!-- /collapse -->
                    </div><!-- /accordion-group -->
                    <br>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                3. Does @include('layouts.partials.title') pay for shortening links?
                            </a>
                        </div>
                        <div id="collapseThree" class="accordion-body collapse in" aria-labelledby="headingThree">
                            <div class="accordion-inner">
                                <p>@include('layouts.partials.title') pays per volume of users that visit the links that you have shortened.</p>
                            </div><!-- /accordion-inner -->
                        </div><!-- /collapse -->
                    </div><!-- /accordion-group -->
                </div><!-- Accordion -->
            </div>

        </div>
    </div><!--/ .container -->
</div><!--/ #faq -->









<div id="top" >
  <span class="glyphicon glyphicon-chevron-up"></span>
</div>




<div id="c">
    <div class="container">
        <p>
            <strong> &copy; {{ date('Y') }} <a href="{{ url('/') }}">@include('layouts.partials.title')</a></strong>
            {{-- | <a href="{{URL::action('PrivacyPolicyController@index')}}">Privacy Policies</a> --}}
            {{-- | <a href="{{URL::action('TermsOfUseController@index')}}">Terms & Conditions</a> --}}
        </p>

    </div>
</div>

<form class="" action="#" method="post">
  <input type="hidden" name="_token" value="{{csrf_token()}}" id="_token">
</form>

{{-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> --}}
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> --}}


<script type="text/javascript">
$(document).ready(function(){
  $("#btn-flat").click(function(event){
    event.preventDefault();
    var urlData=$("#dat").val();
    if (urlData!="url no valida") {
      var uri= '/link'
      var token = $('#_token').val();
      var parametros={
        "url":urlData,
        "_token": token
      }
      $.ajax({
        type : "post",
        url  : uri,
        dataType: "json",
        data: parametros,
        success: function(datos,st){
          $("#dat").val(datos.code);
        }
      });
    }else {
    }


  });

  $("#copy").click(function(){
    var $temp = $("<input>")
    $("body").append($temp);
    $temp.val($("#dat").val()).select();
    document.execCommand("copy");
    $temp.remove();
  });

});

</script>


</body>
</html>
