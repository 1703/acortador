@extends('layouts.app')
@section('contentheader_title')
Settings
@endsection
@section('contentheader_description')
Description
@endsection
@section('csss')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endsection
@section('content')


<div class="box box-success">

@if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> <br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  {{-- <div class="panel-body"> --}}
    {!!Form::open(['route'=>['settings.update'],'method'=>'POST','id'=>'myform'])!!}
      <div class="box-body">
        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            {!!Form::label('name','Name')!!}
            <input type="text" name="names" value="{{$user->id}}" hidden="hidden">
            {!!Form::text('name',$user->name,['class'=>'form-control','id'=>'name','placeholder'=>'Your Name'])!!}
          </div>
        </div>

        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            {!!Form::label('lastname','Last Name')!!}
            {!!Form::text('lastname',$user->lastname,['class'=>'form-control','id'=>'lastname','placeholder'=>'Your LastName'])!!}
          </div>
        </div>

        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            {!!Form::label('ciudad','City')!!}
            {!!Form::text('ciudad',$user->ciudad,['class'=>'form-control','id'=>'ciudad','placeholder'=>'Your City'])!!}
          </div>
        </div>

        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            {!!Form::label('direccion','Address')!!}
            {!!Form::text('direccion',$user->direccion,['class'=>'form-control','id'=>'direccion','placeholder'=>'Your Address'])!!}
          </div>
        </div>

        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            {!!Form::label('codepostal','Postal code')!!}
            {!!Form::text('codepostal',$user->codepostal,['class'=>'form-control','id'=>'codepostal','placeholder'=>'Your ZIP'])!!}
          </div>
        </div>

        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            {!!Form::label('pais_id','Country')!!}
            {!!Form::select('pais_id',$pais,$user->pais_id,['class'=>'form-control','id'=>'pais_id'])!!}
          </div>
        </div>

        <div class="col-lg-6 col-md-6  ">
          <div class="form-group">
            {!!Form::label('email','Email')!!}
            {{-- @if (!empty($email)) --}}
              {!!Form::text('email',$user->email,['class'=>'form-control','id'=>'email','placeholder'=>'youremail@youremail.email','disabled'])!!}
            {{-- @else
              {!!Form::text('email_id',null,['class'=>'form-control','id'=>'','placeholder'=>'youremail@youremail.email'])!!}
            @endif --}}
            <div class="help-block with-errors"></div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 ">
          <div class="form-group">
            {!!Form::label('email_payment','Payment Email')!!}
            @if (!empty($email_payment))
              {!!Form::text('email_payment',$email_payment->email,['class'=>'form-control','id'=>'email_payment','placeholder'=>'youremail@youremail.email'])!!}
            @else
              {!!Form::text('email_payment',null,['class'=>'form-control','id'=>'email_payment','placeholder'=>'youremail@youremail.email'])!!}
            @endif
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            {!!Form::label('metodopago_id','Withdrawal Method')!!}
            {!!Form::select('metodopago_id',$metodopago,null,['class'=>'form-control','required'])!!}
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="form-group text-right">
          {!!Form::submit('Update',['class'=>'btn btn-success','id'=>'update'])!!}
        </div>
      </div>
    {!!Form::close()!!}
  {{-- </div> --}}
</div>
@endsection
@section('scripts')
{{-- <script src="{{asset('/js/bootstrap-validate.js')}}"></script> --}}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#myform').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        name: {
            message: 'The username is not valid',
            validators: {
                notEmpty: {
                    message: 'The username is required and cannot be empty'
                },
                stringLength: {
                    // min: 6,
                    max: 20,
                    message: 'The username must be 20 characters long'
                },
                regexp: {
                    regexp: /^[a-zA-Z0-9_]+$/,
                    message: 'The username can only consist of alphabetical, number and underscore'
                }
            }
        },
        email_payment: {
            validators: {
                notEmpty: {
                    message: 'The email is required and cannot be empty'
                },
                emailAddress: {
                    message: 'The input is not a valid email address'
                }
            }
        },
        lastname: {
            validators: {
              stringLength: {
                  // min: 6,
                  max: 20,
                  message: 'The username must be 20 characters long'
              },
              regexp: {
                  regexp: /^[a-zA-Z0-9_]+$/,
                  message: 'The username can only consist of alphabetical, number and underscore'
              }
            }
        },

        ciudad: {
            validators: {
              stringLength: {
                  // min: 6,
                  max: 20,
                  message: 'The username must be 20 characters long'
              },
              regexp: {
                  regexp: /^[a-zA-Z0-9_]+$/,
                  message: 'The username can only consist of alphabetical, number and underscore'
              }
            }
        },
        direccion: {
            validators: {
              stringLength: {
                  // min: 6,
                  max: 20,
                  message: 'The username must be 20 characters long'
              },
            }
        },
        codepostal: {
            validators: {
              stringLength: {
                  // min: 6,
                  max: 10,
                  message: 'The codepostal must be 10 characters long'
              },
            }
        },
    }
});

});

</script>
@endsection
