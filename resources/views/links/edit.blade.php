@extends('layouts.app')
@section('contentheader_title')
Link
@endsection
@section('contentheader_description')
Edit
@endsection
@section('csss')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endsection
@section('content')
  <div class="box box-info">
    {{-- <div class="box-header "> <!-- with-border -->
      <h3 class="box-title">Links</h3>
    </div> --}}
    <div class="box-body">
      <div class="well well-lg">
        <code>{{url('/',$link->code)}}</code>
      </div>
      {!!Form::open(['route'=>['link.update',$link->id],'method'=>'PUT','id'=>'myform'])!!}
        <div class="box-body">
          <div class="form-group">
            {!!Form::label('url','Link')!!}
            {!!Form::text('url',$link->url,['class'=>'form-control','id'=>'url','placeholder'=>'http://'])!!}
          </div>
        <div class="box-footer">
          <div class="form-group text-right">
            {!!Form::submit('Update',['class'=>'btn btn-success','id'=>'btnupdate'])!!}
          </div>
        </div>
      {!!Form::close()!!}
    </div>
  </div>
@endsection
@section('scripts')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#myform').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          url: {
              message: 'The url is not valid',
              validators: {
                  uri: {
                       message: 'The website address is not valid'
                 }
              }
          },

      }
  });

  });
</script>
@endsection
