@extends('layouts.app')
@section('contentheader_title')
Links
@endsection
@section('contentheader_description')
All links
@endsection
@section('content')

{{-- lista de links --}}
{{$links->render()}}
  <div class="box box-info">
    {{-- <div class="box-header "> <!-- with-border -->
      <h3 class="box-title">Links</h3>
    </div> --}}
    <div class="box-body">
      {{-- <div class="row"> --}}
        <div class="table-responsive no-padding">
          <table class="table table-hover" id="t">
              <thead>
                <tr>
                  <th>Action</th>
                  <th>Link</th>
                  {{-- <th>Views</th> --}}
                  <th>Created</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($links as $link)
                  <tr>
                    <td>
                      <a href="{{url('link/edit',$link->id)}}">
                        <button class="btn btn-primary">
                          <span class="fa fa-pencil" aria-hidden="true"></span>
                        </button>
                      </a>
                      <a href="{{url('link/destroy').'/'.$link->id}}">
                        <button class="btn btn-danger" id="destroy">
                          <span class="fa fa-eraser" aria-hidden="true"></span>
                        </button>
                      </a>
                    </td>
                    <td>
                      <button class="btn btn-sm" id="sell" >
                        <span class="fa fa-clipboard"  aria-hidden="true"></span>
                      </button>
                      <a href="{{url('/'.$link->code)}}" id="foo" target="_blank">{{url('/'.$link->code)}}</a>
                      @if (strlen($link->url)>50) <!-- si el tam es mayor a 50 -->
                        <h5>{{substr($link->url,0,50)." ..."}}</h5>
                      @else
                        <h5>{{ $link->url }}</h5>
                      @endif
                    </td>
                    <td>{{date_format($link->created_at,'Y-m-d') }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table> <!-- table -->
        </div>
      {{-- </div> --}}
    </div>
  </div>
    {{$links->render()}}

@endsection
