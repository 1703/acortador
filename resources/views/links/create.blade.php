@extends('layouts.app')
@section('contentheader_title')
Link
@endsection
@section('contentheader_description')
New
@endsection
@section('csss')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endsection
@section('content')

  @if (!empty(session('status')))
    <div class="alert alert-success">
      <strong>Whoops!</strong> <br><br>
      {{session('status')}}
    </div>
  @endif

  @if (!empty(session('error')))
    <div class="alert  alert-danger">
      <strong>Whoops!</strong> <br><br>
      {{session('error')}}
    </div>
  @endif



  <div class="box box-info">


    {!!Form::open(['route'=>['link.store'],'method'=>'post','id'=>'myform'])!!}
      <div class="box-body">
        <div class="form-group">
          {!!Form::label('url','Link')!!}
          {!!Form::text('url',null,['class'=>'form-control','id'=>'url','placeholder'=>'http://','required'])!!}
        </div>
      </div>
      <div class="box-footer">
          {!!Form::submit('Save',['class'=>'btn btn-success pull-right','id'=>'btnupdate'])!!}
      </div>
    {!!Form::close()!!}
  </div>
@endsection
@section('scripts')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#myform').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          url: {
              message: 'The url is not valid',
              validators: {
                  uri: {
                       message: 'The website address is not valid'
                 }
              }
          },

      }
  });

  });
</script>
@endsection
