
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>ConfirmationCode</title>
  </head>
  <body>
    <h3>@include('layouts.partials.title')</h3>
    <br>
    <p>Hola <b>{{$user->name}}</b>,</p>

    <p>Gracias por crear una cuenta</p>
    <p>Por favor verifica tu cuenta accediendo el siguiente enlace: <br>
      <a href="{{ url('/register/verify/'.$user->confirmation_code) }}">{{  url('/register/verify/'.$user->confirmation_code)   }} </a>
    </p>
  </body>
</html>
