<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>@include('layouts.partials.title') | URL Shortener </title>
  @include('partials.publicidad.antiblock')
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <link rel="shortcut icon" href="/favicon.png">
  <link rel="icon" href="/favicon.png">
  <meta property="og:type" content="website" />

  @include('layouts.publicidad.propeller_scripts')

  
{{-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link href="{{ asset('/css/termsofuse.min.css') }}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&subset=latin" rel="stylesheet" media="all">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="{{ asset('/js/top.min.js')}}"></script>
  {{-- <link href="{{ asset('/css/skins/skin-green.css') }}" rel="stylesheet" type="text/css" /> --}}
  <style media="screen">
    html{
      min-height: 100%;
      position: relative;
    }
    body{
      color: white;
      background-color: #21c600;
      /*margin-bottom: 150px;*/
      /*margin: 0;*/
    }
    .panel{
      height: 250px;
      margin-top: 100px;
      margin-bottom: 300px;
    }
    .footer{
      margin-top: 100px;
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 70px;
    }
    .btn{
      margin-top: 70px;
    }
  </style>
</head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{url('/')}}"><b>@include('layouts.partials.title')</b></a>
            </div>
          </div>
    </div>

    <div id="row">
      <div class="container">
        <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 centered">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title text-center"><b>Error</b></h3>
            </div>
            <div class="panel-body text-center">
              <h3>
              <span class="glyphicon glyphicon-alert"></span>
               Oops!
               <span><a href="{{url('/')}}">help</a></span>
             </h3>
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer class="footer">
      <div class="container">
        <p>
            <strong> &copy;<a href="{{ url('/') }}">@include('layouts.partials.title')</a></strong>
        </p>
      </div>
    </footer>

    {{-- @include('layouts.publicidad.happytag') --}}
  </body>
</html>
