@extends('layouts.app')
@section('contentheader_title')
  Referral
@endsection

@section('content')
<div>
<div class="box box-warning">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="">
          <p>
            The <b>@include('layouts.partials.title')</b> referral program is a great way to spread the word of this great service and to earn even more money with your short links! Refer friends and receive 5% of their earnings for life!
          </p>
        </div>
        <div class="well well-lg">
            <code>{{url('/ref',Auth::user()->idcode) }}</code>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- nav -->
{{-- <h3 class="page-header">Referral Banner</h3>
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#banner1" data-toggle="tab" aria-expanded="true">Home</a></li>
    <li class=""><a href="#banner2" data-toggle="tab" aria-expanded="false">Profile</a></li>
    <li class=""><a href="#banner3" data-toggle="tab" aria-expanded="false">Messages</a></li>
  </ul>
  <div class="tab-content">
    <div id="banner1" class="tab-pane active">
      <b>hola1</b>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>
    <div id="banner2" class="tab-pane">
      <b>hola2</b>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>
    <div id="banner3" class="tab-pane">
      <b>hola3</b>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>
  </div>
</div> --}}
<!-- nav -->

<!-- referrals -->
<h3 class="page-header">My Referrals</h3>
<div class="box box-warning">
  <div class="box-body table-responsive no-padding ">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Username</th>
          <th>Earnings</th>
          <th>My Earnings</th>
        </tr>
      </thead>
      <tbody>
          @if (!empty($referrals))
            @foreach ($referrals as $key => $r)
              <tr>
                <td>{{$r->name}}</td>
                <td>{{$earning[$key]}}</td>
                <td>{{$myearning[$key]}}</td>
              </tr>
            @endforeach
          @else
            <tr>
              <td>No have referrals</td>
              <td></td>
              <td></td>
            </tr>
          @endif
      </tbody>
    </table>
  </div>
</div>
<!-- referrals -->
</div>
@endsection
