<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    {{-- title icono --}}
    @include('layouts.partials.headtitle')
    

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    @include('layouts.publicidad.propeller')
    @include('layouts.publicidad.adsence')
    
    {{-- block adblock --}}
    @include('layouts.publicidad.adblock')
    @include('layouts.publicidad.propeller_scripts')


    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    {{-- <link rel="stylesheet" href="{{asset('/css/app.css')}}"> --}}
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ asset('/css/alt.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&subset=latin" rel="stylesheet" media="all">
    <link href="{{ asset('/css/validate.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('/css/styleindex.css')}}" type="text/css" />
    {{-- <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" /> --}}
    {{-- <link href="{{ asset('/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" /> --}}
    {{-- <link href="{{ asset('/css/skins/skin-green.css') }}" rel="stylesheet" type="text/css" /> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.min.css') }}"> --}}
    {{-- <script type="text/javascript">
      var verifyCallback = function(e) {
        document.getElementById("fmcaptcha").submit()
      };
      var onloadCallback = function() {
        grecaptcha.render("captcha", {
          sitekey: "6LcRPSsUAAAAAGJtsUISh4ykH-UQridxQkZdTt8R",
          callback: verifyCallback
        })
      };
    </script> --}}





{{-- anuncio adsence amp --}}
<script async custom-element="amp-auto-ads"
        src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
</script>

<amp-auto-ads type="adsense"
              data-ad-client="ca-pub-4300688228516282">
</amp-auto-ads>


</head>

<body class="hold-transition login-page" >
  <div class="login-box">
    <div class="login-logo">
      <a href="{{ url('/home') }}"><b> @include('layouts.partials.title') </b></a>
    </div>

     @if (count($errors) > 0)
          <div class="alert alert-danger">
              <strong>Whoops!</strong>  <br><br>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

    <div class="box box-primary"  >
      <div class="box-header with-border text-center">
            <strong>CHECK THE CAPTCHA</strong>
      </div>
      <div class="box-body" align="center">
        <h5 class="text-center">
          Please, check the captcha box to proceed to the destination page.
        </h5>
          <br><br><br>
          {!!Form::open(['url'=>['validate'],'method'=>'POST','id'=>'fmcaptcha'])!!}
          {!!Form::hidden('_token',csrf_token())!!}
          @if (isset($code) && !empty($code))
            {!!Form::hidden('code',$code)!!}
          @endif

          <div class="" style="padding: 30px 5px 30px 0px;">
            <div >
              <div id="captcha"></div>
            </div>
            <div class="g-recaptcha" data-sitekey="6LcRPSsUAAAAAGJtsUISh4ykH-UQridxQkZdTt8R"></div>
          </div>
          <hr>
          {{-- {!!Form::submit('abrir',['class'=>'btn btn-primary btn-sm'])!!} --}}
          {!!Form::submit('GET LINK',['class'=>'btn btn-success btn-lg'])!!}
          {!!Form::close()!!}
          {{-- <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script> --}}
      
      </div>
    </div>
  </div>


  <section>
    <div class="section1">
      <div  class="container">
        <div>
            <h1> MONETIZE YOUR WEBSITE  </h1>
            <h2>Easy way to get extra income from your traffic</h2>
            <p >We will turn your links into earning ones by adding an ad layer. Your visitors will see an ad before reaching a destination page and you will make money.</p>
        </div>
      </div>
    </div>
  </section> 

  <section >
    <div class="section2">
        <div class="row">
          <div>
              <p>@include('layouts.partials.title') is a URL shortening service that allows users to get paid whenever they share links and someone clicks.</p>
            <p>
                We pay for ALL visitor you bring to your links and payout at least $@include('layouts.partials.usdmin') per 1000 views. Multiple views from the same viewer are also counted thus you will be benefiting from all your traffic.
            </p>
          </div>
          <hr>
          <div>
              <h1>Shorten URLs and earn money</h1>
              <p>
                  Signup. Once you've completed your registration just start creating short URLs and sharing the links with your family and friends.
              </p>
              <div>
                <div class="btn btn-danger btn-lg">
                  <a href="{{url('/register')}}"> JOIN NOW!</a>
                </div>
              </div>
          </div>
        </div>
    </div>
  </section>

<footer class="footer">
  <div id="c">
    <p>
        <strong> &copy; {{ date('Y') }} <a href="{{ url('/') }}">@include('layouts.partials.title')</a></strong>
        <span> | </span>
        <a href="{{URL::action('PrivacyPolicyController@index')}}">Privacy Policies</a>
        <span> | </span>
        <a href="{{URL::action('TermsOfUseController@index')}}">Terms & Conditions</a>
    </p>
  </div>
</footer>




{{-- <script src="{{asset('/js/app.js')}}" charset="utf-8"></script>
<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script> --}}


{{--  <script type="text/javascript">
var onloadCallback = function () {
    grecaptcha.render('dvCaptcha', {
        'sitekey': '6LcRPSsUAAAAAGJtsUISh4ykH-UQridxQkZdTt8R',
        'callback': function (response) {
          colose.log("callback:"+response);
            // $.ajax({
            //     type: "POST",
            //     url: "Default.aspx/VerifyCaptcha",
            //     data: "{response: '" + response + "'}",
            //     contentType: "application/json; charset=utf-8",
            //     dataType: "json",
            //     success: function (r) {
            //         var captchaResponse = jQuery.parseJSON(r.d);
            //         if (captchaResponse.success) {
            //             $("[id*=txtCaptcha]").val(captchaResponse.success);
            //             $("[id*=rfvCaptcha]").hide();
            //         } else {
            //             $("[id*=txtCaptcha]").val("");
            //             $("[id*=rfvCaptcha]").show();
            //             var error = captchaResponse["error-codes"][0];
            //             $("[id*=rfvCaptcha]").html("RECaptcha error. " + error);
            //         }
            //     }
            // });
        }
    });
};
</script>
 --}}


{{--
<script>
$(document).ready(function(e) {

$("#fmcaptcha").submit(function(e) {
    $respuesta=false;
    $.ajax({
    url: "{{ url('captcha') }}",
    type:"POST",
    dataType:"json",
    data:$("#fmcaptcha").serialize(),
    async:false,
    success:
        function(msg){
           $respuesta=msg.success;
           if($respuesta) {
            $("#g-recaptcha-response","#fmcaptcha").remove();
           } else    {
              alert('Error');
           }
    },
    error:
        function (xhr, ajaxOptions, thrownError){
            alert('Url: '+this.url+'\n\r’+’Error: '+thrownError);
        }
    });
    return $respuesta;
    });
});
 </script>  --}}


</body>
</html>
