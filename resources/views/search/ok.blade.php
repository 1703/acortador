<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    {{-- title icono --}}
    @include('layouts.partials.headtitle')
    
    @include('layouts.publicidad.propeller')
    @include('layouts.publicidad.adsence')
    {{-- block adblock --}}
    @include('layouts.publicidad.adblock')
    {{-- propeller popunder scripts--}}
    <script type="text/javascript" src="//deloplen.com/apu.php?zoneid=2709080" async data-cfasync="false"></script>
    {{-- native subscriptions --}}
    <script src="//pushqwer.com/ntfc.php?p=2699475" data-cfasync="false" async></script>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    {{-- <link rel="stylesheet" href="{{asset('/css/app.css')}}"> --}}
    {{-- <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" /> --}}
    {{-- <script src='https://www.google.com/recaptcha/api.js'></script> --}}
    {{-- <link href="{{ asset('/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" /> --}}
    {{-- <link href="{{ asset('/css/skins/skin-green.css') }}" rel="stylesheet" type="text/css" /> --}}
     {{-- <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.min.css') }}"> --}}

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <link href="{{ asset('/css/alt.min.css') }}" rel="stylesheet" type="text/css" />
     <link href="{{ asset('/css/validate.css') }}" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="{{asset('/css/styleindex.css')}}" type="text/css" />

     <style media="screen">
           #st{
             height: 150px;
             width: 150px;
             border: 2px solid rgb(255, 0, 0);
             color: rgb(255, 0, 0);
             border-radius: 100%;
             font-size: 2em;
           }
           #time{
             margin: 15%;
           }

         </style>

  

</head>

<body class="hold-transition login-page" >
  <div class="login-box">
    <div class="login-logo">
      <a href="{{ url('/home') }}"><b> @include('layouts.partials.title') </b></a>
    </div>


     @if (count($errors) > 0)
          <div class="alert alert-danger">
              <strong>Whoops!</strong>  <br><br>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

    <div class="box box-primary"  >
      <div class="box-header with-border text-center">
            <strong>YOUR LINK IS ALMOST READY</strong>
      </div>
      <div class="box-body"  align="center">
        {{-- <br>
        <span >
          <i id="spi" class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>
        </span>
        <br>
        <br> --}}
        <br>
        <div id="st">
          <div id="time">5</div>
          <div id="seconds">Seconds</div>
        </div>
        <br>
        <br>
        <hr>

        {!!Form::open(['url'=>['ok'],'method'=>'POST','id'=>'fm'])!!}
          <div class="">
            {!!Form::hidden('_token',csrf_token())!!}
            @if (isset($code) && !empty($code))
              {!!Form::hidden('code',$code)!!}
            @endif
          </div>
          {{-- <a href="#" class="btn btn-success btn-lg" id="s">GET LINK</a> --}}
          <button class="btn btn-danger btn-lg" id="s" type="submit" name="button" disabled="disabled">GET LINK</button>

        {!!Form::close()!!}
      </div>
    </div>
  </div>

  @include('layouts.publicidad.anuncio_in-feed_solotexto')



  <section class="section1">
      <div  class="container">
        <div>
            <h1> MONETIZE YOUR WEBSITE  </h1>
            <h2>Easy way to get extra income from your traffic</h2>
            <p >We will turn your links into earning ones by adding an ad layer. Your visitors will see an ad before reaching a destination page and you will make money.</p>
        </div>
      </div>
    </section> 
  
    <section class="section2">
      <div class="row">
          <div>
            <p>@include('layouts.partials.title') is a URL shortening service that allows users to get paid whenever they share links and someone clicks.</p>
            <p>
                We pay for ALL visitor you bring to your links and payout at least $@include('layouts.partials.usdmin') per 1000 views. Multiple views from the same viewer are also counted thus you will be benefiting from all your traffic.
            </p>
          </div>
          <hr>
          <div>
              <h1>Shorten URLs and earn money</h1>
              <p>
                  Signup. Once you've completed your registration just start creating short URLs and sharing the links with your family and friends.
              </p>
              <div>
                <div class="btn btn-danger btn-lg">
                <a href="{{url('/register')}}"> JOIN NOW!</a>
                </div>
              </div>
          </div>
      </div>
    </section>


  <footer class="footer">
    <div id="c">
      <p>
          <strong> &copy; {{ date('Y') }} <a href="{{ url('/') }}">@include('layouts.partials.title')</a></strong>
          <span> | </span>
          <a href="{{URL::action('PrivacyPolicyController@index')}}">Privacy Policies</a>
          <span> | </span>
          <a href="{{URL::action('TermsOfUseController@index')}}">Terms & Conditions</a>
      </p>
    </div>
  </footer>


{{-- <script src="{{asset('/js/app.js')}}" charset="utf-8"></script> --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
{{-- <script >
$(document).ready(function(){
  $('#s').attr('disabled', true);
  $.ajax({
    type: "POST",
    url: "{{ url('ok') }}",
    data:$("#fm").serialize(),
    dataType: "json",
    success: function(msg){
      console.log(msg);
               $('#s').attr('disabled', false);
               $('#spi').removeClass('fa fa-spinner fa-pulse fa-5x fa-fw').addClass('fa fa-spinner fa-5x fa-fw');
               $('#s').attr('href',msg.li);
             }

  });
});
</script> --}}
<script type="text/javascript">
  $(document).ready(function() {
    (function(){
      var cont=5;
      $('#time').text(cont);
      var func=function(){
        --cont;
        $('#time').text(cont);
        if (cont==0) {
          $('#s').removeClass('btn btn-danger btn-lg');
          $('#s').addClass('btn btn-success btn-lg');
          $('#st').css({
            'border': '2px solid #188725',
            'color': '#188725',
          });
          $('#s').attr({'disabled': false});
          clearInterval(interv);
        }
      };
      var interv=setInterval(func,1000);
    })();
  });
</script>

</body>
</html>
