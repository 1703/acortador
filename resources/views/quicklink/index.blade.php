@extends('layouts.app')
@section('contentheader_title')
  Quick Link
@endsection
@section('contentheader_description')
  quick link
@endsection

@section('content')
<div>
<div class="box box-danger">
  <div class="box-body">
        <div class="">
          <p>
            For developers ByteLy prepared API which returns responses in JSON or TEXT formats.
          </p>
        </div>
        <br>
        <div class="well well-lg">
            <code>
              {!! url('/').'/q/<b>'.Auth::user()->idcode.'</b>/?s=http://www.yoururl.com' !!}
            </code>
        </div>
  </div> <!-- panel body -->
</div>
@endsection
