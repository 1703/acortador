<!DOCTYPE html>
<html lang="en">
@include('layouts.partials.htmlheader')

<body data-spy="scroll"  class="skin-green">
<div id="navigation" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=""><b>@include('layouts.partials.title')</b></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">

            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}"><b>Login</b></a></li>
                    <li><a href="{{ url('/register') }}"><b> Register </b></a></li>
                @else
                    <li><a href="{{url('/home')}}"><b>{{ Auth::user()->name }}</b></a></li>
                @endif
            </ul>
        </div>
    </div>
</div>




{{-- <section id="home" name="home"></section> --}}
<section class="home" id="home">
    <div class="container ">
        <div class="row ">
            <div class="col-lg-12 text-center">
                <h1 >
                    <b>THE BEST URL SHORTENER</b>
                </h1>
                <p class="h4"> Sharing links on the internet. </p>
                <p class="h3"> Get paid for every person who visits your URLs </p>
            </div>
            <div class="col-sm-12 col-md-10 col-md-offset-1" id="short"  >
                <form action="{{ route('link.stor') }}" id="myform" method="post">
                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="input-group input-group-lg"  >
                        <span class="input-group-btn" >
                            <button class="btn btn-warning btn-flat " type="button" id="copy">
                              <i class='fa fa-copy'></i>
                            </button>
                        </span>
                        <input name="url" id="dat" type="text" class="form-control" placeholder="http://www.yourlink..." value="" required />
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-flat" id="btn-flat" type="submit">
                              GO!</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div> <!--/ .container -->
</section>

{{-- <hr class="container"> --}}





{{-- <section id="showcase" name="showcase"></section> --}}
<section class="showcase" id="showcase" >
    <div class="container">
        <div class="row ">
            <h1 >
              {{-- <b>Use our shortened service</b> --}}
              <b>USE OUR SHORTENED SERVICE</b>
            </h1>
            <h4>Sign up using your email</h4>
            <div class="col-lg-8 col-lg-offset-2  singup">
                <div class="btn btn-danger btn-lg"  >
                    <a href="{{url('/register')}}">SIGN UP NOW!</a>
                </div>
            </div>
        </div>
    </div><!-- /container -->
</section>

{{-- <section id="about" name="about"></section> --}}
<section class="about" id="about" >
    <div class="container">
        <div class="row ">
            {{-- <h1 class="text-center" ><b>ABOUT ACORTLINK</b></h1> --}}
            <div class="text-center">

              <div class="col-lg-6">
                <h2>What is the @include('layouts.partials.title') </h2>
                <p>
                  @include('layouts.partials.title') is a great tool for any user looking to earn money by shortening their URLs.
                </p>
              </div>
              <div class="col-lg-6">
                <h2>Statistics </h2>
                <p>
                  You can see at all time the statistics of your links, perform a daily control of each link visit and see the money generated.
                </p>
              </div>
            </div>
{{--
            <div class="col-lg-6 centered">
                <img class="centered" src="{{ asset('/img/mobile.png') }}" alt="">
            </div> --}}
        </div>
    </div><!--/ .container -->
</section>


{{-- <section id="desc" name="desc"></section> --}}
<section class="desc" id="desc">
    <div class="container">
        <div class="row">
            <div  class="desc-estil col-lg-4  col-sm-12 col-md-12 user">
                <span id="ico" class="fa fa-user"></span>
                <p><span class="h3">Create an Account</span></p>
            </div>
            <div  class="desc-estil col-lg-4 col-sm-12 col-md-12">
                <span id="ico" class="fa fa-chain"></span>
                <p><span class="h3">Shorte Your Link</span></p>
                <p><span class="h5">Add URLs and shorten your links to monetize.</span></p>

            </div>
            <div class="desc-estil col-lg-4 col-sm-12 col-md-12">
                <span id="ico" class="fa fa-money"></span>
                <p><span class="h3">Earn Money</span></p>
                <p><span class="h5">Get paid for every real person who visit your links.</span></p>
            </div>
        </div>
    </div> <!-- / .container  -->
</section>


{{-- <section id="faq" name="faq"></section> --}}
<section class="faq" id="faq">
    <div class="container">
        <div class="row">
            <h1 class="text-center"><b>FREQUENTLY ASKED QUESTIONS</b></h1>
            <br>
            <br>
            <div class="col-lg-6 image text-center">
                <img src="{{ asset('/img/mobile.png') }}" alt="">
            </div>

            <div class="col-lg-6">
                <div class="accordion ac" id="accordion" aria-multiselectable="true">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              <span>1.</span>
                               What is the minimum payment at @include('layouts.partials.title')?
                            </a>
                        </div>
                        <div id="collapseOne" class="accordion-body collapse" aria-labelledby="headingOne">
                            <div class="accordion-inner">
                              <br>
                                <p>@include('layouts.partials.title') will make the payment when users have earned a sum of $ @include('layouts.partials.usd'). Once users have accrued $ @include('layouts.partials.usd'), they will receive the payment. </p>
                            </div><!-- /accordion-inner -->
                        </div><!-- /collapse -->
                    </div><!-- /accordion-group -->
                    <br>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              <span>2.</span>
                                When does @include('layouts.partials.title') pay ?
                            </a>
                        </div>
                        <div id="collapseTwo" class="accordion-body collapse" aria-labelledby="headingTwo">
                            <div class="accordion-inner">
                              <br>
                                <p>
                                    Here’s a reason to celebrate! @include('layouts.partials.title') pays users every fortnight. Payments are made on the 1st and 16th day of the month. See below for an example:
                                    <br>
                                    - All money generated between the 1st and the 15th of January will be credited to the user’s account on the 16th of January.
                                    <br>
                                    - All money generated between the 16th and the 31st of January will be credited to the user’s account on the 1st of February.
                                    <br>
                                </p>
                            </div><!-- /accordion-inner -->
                        </div><!-- /collapse -->
                    </div><!-- /accordion-group -->
                    <br>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              <span>3.</span>
                                Does @include('layouts.partials.title') pay for shortening links?
                            </a>
                        </div>
                        <div id="collapseThree" class="accordion-body collapse" aria-labelledby="headingThree">
                            <div class="accordion-inner">
                                <br>
                                <p>@include('layouts.partials.title') pays per volume of users that visit the links that you have shortened.</p>
                            </div><!-- /accordion-inner -->
                        </div><!-- /collapse -->
                    </div><!-- /accordion-group -->
                </div><!-- Accordion -->
            </div>

        </div>
    </div><!--/ .container -->
</section><!--/ #faq -->









<div id="top" >
  <span class="glyphicon glyphicon-chevron-up"></span>
</div>




<footer class="footer">
    <div class="container">
        <p>
            {{-- <strong> &copy; {{ date('Y') }} <a href="{{ url('/') }}">@include('layouts.partials.title')</a></strong> --}}
            <strong>Copyright &copy; 2017-{{date('Y')}} </strong>   @include('layouts.partials.title')
            <span> | </span>
            <a href="{{ route('privacy.index')}}">Privacy Policies</a>
            <span> | </span>
            <a href="{{ route('terms.index')}}">Terms & Conditions</a>
        </p>
    </div>
</footer>

<form class="" action="#" method="post">
  <input type="hidden" name="_token" value="{{csrf_token()}}" id="_token">
</form>

{{-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> --}}
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> --}}


<script type="text/javascript">
$(document).ready(function(){

  $("#copy").click(function(){
    var $temp = $("<input>")
    $("body").append($temp);
    $temp.val($("#dat").val()).select();
    document.execCommand("copy");
    $temp.remove();
  });

  $('#btn-flat').click(function(e){
    e.preventDefault();
    var form = $('#myform');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '{!! route('link.stor') !!}',
        type: 'post',
        dataType: 'json',
        data: form.serialize(),
        headers:{
            "X-CSRF-TOKEN": CSRF_TOKEN,
        } ,
        // data: {param1: 'value1'},
    })
    .done(function(data,success) {
      console.log(data);
      $('#dat').val(data.code);
      window.location.href= data.code;
    })
    .fail(function(e) {
        // console.log(e);
    })
    .always(function() {
        // console.log("complete");
    });
  });



});

</script>




</body>
</html>
