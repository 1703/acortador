<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  @include('layouts.partials.headtitle')

  <link rel="stylesheet" href="{{asset('css/auth.css')}}">

  {{-- <!-- Bootstrap 3.3.7 --> --}}
  {{-- <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css"> --}}
  {{-- <!-- Font Awesome --> --}}
  {{-- <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css"> --}}
  {{-- <!-- Ionicons --> --}}
  {{-- <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css"> --}}
  {{-- <!-- Theme style --> --}}
  {{-- <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css"> --}}
  {{-- <!-- iCheck --> --}}
  <link rel="stylesheet" href="{{asset('plugins/iCheck/square/green.css')}}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>


  <link href="{{ asset('/css/main.min.css') }}" rel="stylesheet">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  @include('layouts.publicidad.adsence')
  @include('layouts.publicidad.propeller')
  
  {{-- propeller native subscriptions--}}
  <script src="//pushlum.com/ntfc.php?p=2701001" data-cfasync="false" async></script>
  
  {{-- propeller popunder --}}
  {{-- <script type="text/javascript" src="//deloplen.com/apu.php?zoneid=2709080" async data-cfasync="false"></script> --}}

</head>
