<!DOCTYPE html>
<html lang="en">

@include('layouts.partials.htmlheader')

<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  @include('layouts.partials.header')
  <aside class="main-sidebar">
    @include('layouts.partials.sidebar')
  </aside>
  <div class="content-wrapper">
    @include('layouts.partials.contentheader')
    <section class="content">
      @yield('content')
    </section>
  </div>
  @include('layouts.partials.footer')

  {{-- @include('layouts.partials.controlsidebar') --}}

</div>

 @include('layouts.partials.scripts')
 @yield('scripts')
</body>
</html>
