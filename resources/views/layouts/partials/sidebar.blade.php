<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  {{-- <!-- Sidebar user panel --> --}}
  <div class="user-panel">
    <div class="pull-left image">
      <img src="{{  !empty(\Auth::check()) ?Gravatar::get(\Auth::user()->email) : Gravatar::get('cualquiermail@example.com')  }}" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
      <p>
          {{   !empty(\Auth::check())? \Auth::user()->name: "Anonymous"   }}
       </p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  <!-- search form -->
  {{-- <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
    </div>
  </form> --}}
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">MENU</li>
    <li {{Request::is('home')?'class=active':''}}>
      <a href="{{url('home')}}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
      </a>
    </li>

    {{-- <li class=" ">
      <a href="{{url('/links')}}">
        <i class="fa fa-link"></i> <span>Links</span>
      </a>
    </li> --}}


    @if (Request::is('link') == true || Request::is('link/create') == true)
      <li class="treeview menu-open active">
        <a href="#">
          <i class="fa fa-link"></i>
          <span>Links</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu" style="display: block;">
          <li><a href="{{route('link.create')}}"><i class="fa  fa-angle-right"></i>New link</a></li>
          <li><a href="{{route('link.index')}}"><i class="fa  fa-angle-right"></i>Links</a></li>
        </ul>
      </li>
    @else
      <li class="treeview">
        <a href="#">
          <i class="fa fa-link"></i>
          <span>Links</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('link.create')}}"><i class="fa  fa-angle-right"></i>New link</a></li>
          <li><a href="{{route('link.index')}}"><i class="fa  fa-angle-right"></i>Links</a></li>
        </ul>
      </li>
    @endif





    {{-- <li class="">
      <a href="{{route('quicklink.index')}}">
        <i class="fa fa-share"></i> <span>QuickLink</span>
      </a>
    </li> --}}

    @if (\Auth::user()->isadmin)
    <li {{Request::is('users')?'class=active':''}}>
        <a href="{{route('users.index')}}">
          <i class="fa fa-users"></i> <span>Users</span>
        </a>
      </li>
    @endif

    <li {{Request::is('api')?'class=active':''}}>
      <a href="{{route('api.index')}}">
        <i class="fa fa-gears"></i> <span>Api</span>
      </a>
    </li>

    <li {{Request::is('referral')?'class=active':''}}>
      <a href="{{route('referral.index')}}">
        <i class="fa fa-users"></i> <span>Referrals</span>
      </a>
    </li>

    <li {{Request::is('payments')?'class=active':''}}>
      <a href="{{route('payments.index')}}">
        <i class="fa fa-usd"></i> <span>Payments</span>
      </a>
    </li>

    <li {{Request::is('settings')?'class=active':''}}>
      <a href="{{route('settings.index')}}">
        <i class="fa fa-cog"></i> <span>Settings</span>
      </a>
    </li>
  </ul>
</section>
<!-- /.sidebar -->
