<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @include('layouts.partials.headtitle')

  {{-- <!-- Tell the browser to be responsive to screen width --> --}}
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  {{-- <!-- Bootstrap 3.3.7 --> --}}
  {{-- <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css"> --}}
  {{-- <!-- Font Awesome --> --}}
  {{-- <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css"> --}}
  {{-- <!-- Ionicons --> --}}
  {{-- <link rel="stylesheet" href="node_modules/Ionicons/css/ionicons.min.css"> --}}
  {{-- <!-- Theme style --> --}}
  {{-- <link rel="stylesheet" href="dist/css/AdminLTE.min.css"> --}}

  {{-- <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css"> --}}
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  {{-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'> --}}
  {{-- <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'> --}}


  {{-- <!-- Morris chart --> --}}
  {{-- <link rel="stylesheet" href="node_modules/morris.js/morris.css"> --}}
  {{-- <!-- jvectormap --> --}}
  {{-- <link rel="stylesheet" href="node_modules/jvectormap/jquery-jvectormap.css"> --}}
  {{-- <!-- Date Picker --> --}}
  {{-- <link rel="stylesheet" href="node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"> --}}
  {{-- <!-- Daterange picker --> --}}
  {{-- <link rel="stylesheet" href="node_modules/bootstrap-daterangepicker/daterangepicker.css"> --}}
  {{-- <!-- bootstrap wysihtml5 - text editor --> --}}
  {{-- <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"> --}}

  {{-- <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --> --}}
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  {{-- <!-- Google Font --> --}}
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

{{-- propeller native subscriptions --}}
{{-- <script src="//pushlum.com/ntfc.php?p=2701001" data-cfasync="false" async></script>
<script type="text/javascript" src="//deloplen.com/apu.php?zoneid=2709080" async data-cfasync="false"></script> --}}


  {{-- landing --}}
  @if (request()->url()==url('/'))
    {{-- <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&subset=latin" rel="stylesheet" media="all"> --}}
    {{-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link href="{{ asset('/css/landing.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/top.min.css')}}" rel="stylesheet"></link>
    <script src="{{ asset('/js/top.min.js')}}"></script>
  @endif

  @if (request()->url()==url('/terms') || request()->url()==url('/privacy'))
      {{-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> --}}
      {{-- <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.min.css') }}"> --}}
      {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> --}}
      {{-- <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&subset=latin" rel="stylesheet" media="all"> --}}
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <link href="{{ asset('/css/top.min.css')}}" rel="stylesheet"></link>
      <script src="{{ asset('/js/top.min.js')}}"></script>
      <link href="{{ asset('/css/termsofuse.min.css') }}" rel="stylesheet">
    @endif

    {{-- publicidad --}}
    @include('layouts.publicidad.adsence')
    @include('layouts.publicidad.propeller')

    {{-- native suscriptions --}}
    <script src="//pushqwer.com/ntfc.php?p=2701001" data-cfasync="false" async></script>
    <script src="//pushqwer.com/ntfc.php?p=2699475" data-cfasync="false" async></script>

    @yield('csss')
</head>
