<header class="main-header">
  <!-- Logo -->
  <a href="{{url('/')}}" class="logo" id="logo">
    {{-- <!-- mini logo for sidebar mini 50x50 pixels --> --}}
    <span class="logo-mini"><b>AL</b></span>
    {{-- <!-- logo for regular state and mobile devices --> --}}
    <span class="logo-lg"><b>@include('layouts.partials.title')</b></span>

  </a>
  {{-- <!-- Header Navbar: style can be found in header.less --> --}}
  <nav class="navbar navbar-static-top">
    {{-- <!-- Sidebar toggle button--> --}}
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        {{-- <!-- Messages: style can be found in dropdown.less--> --}}
        {{-- @include('layouts.partials.messages') --}}
        {{-- <!-- Notifications: style can be found in dropdown.less --> --}}
        {{-- @include('layouts.partials.notifications') --}}
        {{-- <!-- Tasks: style can be found in dropdown.less --> --}}
        {{-- @include('layouts.partials.tasks') --}}
        {{-- <!-- User Account: style can be found in dropdown.less --> --}}
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ !empty(\Auth::check()) ?Gravatar::get(\Auth::user()->email) : Gravatar::get('cualquiermail@example.com') }}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{ !empty(\Auth::check())? \Auth::user()->name: "Anonymous"  }}</span>
          </a>
          <ul class="dropdown-menu">
            {{-- <!-- User image --> --}}
            <li class="user-header">
              <img src="{{!empty(\Auth::check()) ?Gravatar::get(\Auth::user()->email) : Gravatar::get('cualquiermail@example.com')}}" class="img-circle" alt="User Image">

              <p>
                {{ !empty(\Auth::check())? \Auth::user()->name: "Anonymous" }}
                <small>Member since {{  !empty(\Auth::check())? date_format(\Auth::user()->created_at,"Y-m-d") : date('Y-m-d')}}</small>
              </p>
            </li>
            {{-- <!-- Menu Body --> --}}
            {{-- <li class="user-body">
              <div class="row">
                <div class="col-xs-4 text-center">
                  <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Friends</a>
                </div>
              </div> --}}
              <!-- /.row -->
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{url('/settings')}}" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="{{url('/logout')}}" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
        {{-- <!-- Control Sidebar Toggle Button --> --}}
        {{-- <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li> --}}
      </ul>
    </div>
  </nav>
</header>
