<section class="content-header">
  <h1>
    {{-- Dashboard --}}
    @yield('contentheader_title')
    <small> @yield('contentheader_description')</small>
  </h1>
  {{-- <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">@yield('contentheader_title')</li>
  </ol> --}}
</section>
