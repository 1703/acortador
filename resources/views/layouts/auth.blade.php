<!DOCTYPE html>
<html>
@include('layouts.auth.header')
<body class="hold-transition login-page">
  {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-4300688228516282"
     data-ad-slot="5352859959"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script> --}}




  <div class="register-box  ">
    <div class="text-center">
      @include('layouts.auth.logo')
      @if (count($errors) > 0)
        <div class="alert alert-danger ">
          <strong>Whoops!</strong> <br><br>
          <ul style="list-style:none;" >
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
    </div>
    @yield('content')
  </div>


  <footer class="footer">
    <div class="container text-center">
            {{-- <strong> &copy; {{ date('Y') }} <a href="{{ url('/') }}">@include('layouts.partials.title')</a></strong> --}}
            <strong>Copyright &copy; 2017-{{date('Y')}} </strong>  @include('layouts.partials.title')
            <span>|</span>
            <a href="{{ route('privacy.index')}}">Privacy Policies</a>
            <span>|</span>
            <a href="{{ route('terms.index')}}">Terms & Conditions</a>
    </div>
  </footer>
  {{-- <div id="c"> --}}

  {{-- </div> --}}
  @include('layouts.auth.scripts')



  <!-- acortlink_display_horizontal -->
  {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <ins class="adsbygoogle"
       style="display:block"
       data-ad-client="ca-pub-4300688228516282"
       data-ad-slot="9088677255"
       data-ad-format="auto"
       data-full-width-responsive="true"></ins>
  <script>
       (adsbygoogle = window.adsbygoogle || []).push({});
  </script>




  <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <!-- acortlink_ads_link_horizontal -->
  <ins class="adsbygoogle"
       style="display:block"
       data-ad-client="ca-pub-4300688228516282"
       data-ad-slot="5935330456"
       data-ad-format="link"
       data-full-width-responsive="true"></ins>
  <script>
       (adsbygoogle = window.adsbygoogle || []).push({});
  </script>


  <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script>
       (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-4300688228516282",
            enable_page_level_ads: true
       });
  </script> --}}

</body>
</html>
