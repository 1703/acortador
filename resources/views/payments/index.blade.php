@extends('layouts.app')
@section('contentheader_title')
Payments
@endsection
@section('contentheader_description')
@endsection

@section('content')
 <div class="box box-danger">
   <div class="box-body">
     <div class="col-md-12">
       <div class="small-box bg-green">
         <div class="inner">
           <h4 class="text-center">
              ${{$earning_mes}} + ${{$earning_referrals}} = ${{$ganancia_total}}
             {{-- <sup style="font-size:20px">%</sup> --}}
           </h4>
           <p class="text-center">Total own income + Total referred income = Total Available Earnings</p>
           {{-- <small class="h5">Total own income + Total referred income = Total Available Earnings</small> --}}
         </div>
         <div class="icon">
           {{-- <i class="ion ion-stats-bars"></i> --}}
           <i class="fa fa-money"></i>
         </div>
       </div>
     </div>
     {{-- <div class="col-md-6">
       <div class="small-box bg-red">
         <div class="inner">
           <h4>
             0
            <!--   <sup style="font-size:20px">%</sup> -->
           </h4>
           <p>Last Pay Period</p>
           <!--
           <small class="h5">Last Pay Period</small>
           -->
         </div>
         <div class="icon">
           <i class="ion ion-pie-graph"></i>
         </div>
       </div>
     </div> --}}
   </div>
</div>

<hr>
<div class="row">
 <div class="col-md-4 text-center">
   <span class="h4">Payment Processor</span> <br>
     <small class="h5 text-success">
       <i class="fa fa-edit"></i>
       @if (!empty($metodopago))
         {{$metodopago}}
       @else
         <a href="{{url('panel/settings')}}"> payment</a>
       @endif
     </small>
 </div>
 <div class="col-md-4 text-center">
   <span class="h4">Payment Email</span> <br>
     <small class="h5 text-success">
       <i class="fa fa-edit"></i>
       @if (isset($email) && !empty($email))
         {{$email}}
       @else
         <a href="{{url('panel/settings')}}">email@email.com</a>
       @endif
     </small>
 </div>
 <div class="col-md-4 text-center">
   <span class="h4">Next Payment date</span> <br>
   <small class="h5 text-success">
     <i class="fa fa-edit"></i>
     @if (!empty($fechapago))
       {{ $fechapago}}
     @else
       {{ date('Y-m-d')}}
     @endif

   </small>
 </div>
</div>
<hr>
<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Transaction History</h3>
  </div>
  <div class="box-body">
    <div class="box-body table-responsive no-padding">
      <table class="table table-hover">
        <tbody>
          <tr>
            <th>Payment date</th>
            <th>Payment processor</th>
            <th>Email</th>
            <th>Status</th>
            <th>Amount</th>
          </tr>
          @foreach ($pagos as $p)
            {{-- los pagos con estado creado no se muestran --}}
            @if ($p->estado!='creado')
              <tr>
                <td>{{$p->fechapago_fin}}</td>
                <td>{{$p->metodopago}}</td>
                @if (empty($p->email))
                  <td><a href="{{url('panel/settings')}}">email</a></td>
                @else
                  <td>{{$p->email}}</td>
                @endif
                @if ($p->estado=="pagado")
                  <td >
                    <span class="btn-success btn-xs">{{$p->estado}}</span>
                  </td>
                @else
                  <td>
                    <span class="btn-warning btn-xs">{{$p->estado}}</span>
                  </td>
                @endif
                <td>{{$p->monto}}</td>
              </tr>
            @endif
          @endforeach
            {{-- @foreach ($referrals as $key => $r)
              <tr>
                <td>{{$r->name}}</td>
                <td>{{$earning[$key]}}</td>
                <td>{{$myearning[$key]}}</td>
              </tr>
            @endforeach --}}
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection
