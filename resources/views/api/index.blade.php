@extends('layouts.app')
@section('contentheader_title')
  Api
@endsection
@section('contentheader_description')
  developers API
@endsection

@section('content')
<div>
<div class="box box-danger">
  <div class="box-body">
        <div class="">
          <p>
            For developers ByteLy prepared API which returns responses in JSON or TEXT formats.
          </p>
        </div>
        <br>
        <div class="well well-lg">
            <code>
              {!! url('/').'/api/link/?<b>token</b>='.Auth::user()->idcode.'&<b>uri</b>=http://www.yoururl.com' !!}
            </code>
        </div>

          <p>
            You will get a JSON response like the following
          </p>
        <div class="well well-lg">
          <code><br>
            {<br>
              data:{<br>
              &nbsp;&nbsp; status: 'ok',<br>
              &nbsp;&nbsp; msj: '{{ url('/',Auth::user()->idcode) }}'<br>
              &nbsp;&nbsp;}<br>
            }<br>
          </code>
        </div>
        <p>Using the API in PHP</p>

        <div class="well well-lg">
          <code> <br>
              $link &nbsp;&nbsp;&nbsp;= '{{ url('/') }}/api/link/?<b>token</b>={{Auth::user()->idcode}}&<b>uri</b>=http://www.yourlink.com';<br>
              @$result = json_decode(file_get_contents($link),true);<br>
              foreach ($result as $data => $value) {<br>
                &nbsp;&nbsp;&nbsp;echo $value['msj'];<br>
              }<br>
          </code>
        </div>
  </div> <!-- panel body -->
</div>
@endsection
