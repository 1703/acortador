<!DOCTYPE html>
<html lang="en">
@include('layouts.partials.htmlheader')

<body >
  <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
          <div class="navbar-header">
              <a class="navbar-brand" href="{{url('/')}}"><b>@include('layouts.partials.title')</b></a>
          </div>
      </div>
  </div>




<div id="rows" >
  <div class="container">
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 centered">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title text-center"><strong>PRIVACY POLICY</strong></h3>
          </div>
          <div class="panel-body">
            <table>
                <tbody>
                  <tr>
                    <td>
                      <div align="justify">
                        <br>
                        The term '@include('layouts.partials.title')' or 'us' or 'we' refers to the owner of the website '@include('layouts.partials.title')'. The term 'you' refers to the user or viewer of our website. The term 'Content' refers to the website you are using in conjunction with the Service.
                        <br><br>
                          We at @include('layouts.partials.title') respect the privacy of our visitors and are committed to preserving your online safety by preserving your privacy at anytime you visit or communicate with our site. Our privacy policy has been provided and reviewed by the solicitors at Legal Centre who specialise in online internet contracts.
                        <br><br>
                          Our Terms of Use and Privacy Policy offers you a thorough explanation regarding your personal data provided to us or any data we may collect from you.
                        <br><br>
                          Both the Privacy Policy and the Terms and Conditions are updated from time to time and therefore should review them.
                        <br><br>
                        <h4><b>1. Information obtained</b></h4>
                        <br>
                          @include('layouts.partials.title') can collect and process data from the operations performed on our website, below explain the ways in which we can carry out these actions:
                        <br><br>
                          a. Data collected your visit to our website and the resources used to include it in the location data, weblogs, traffic data and any other information communication.
                        <br><br>
                          b. If you communicate at any time with our staff on the Site, we can collect information.
                        <br><br>
                        <h4><b>2. Use of Cookies</b></h4>
                        <br>
                        There are cases where we can use cookies to collect information about our services for our website and for our advertisers.
                        <br><br>
                        At no time will any information collected identification data, these data will be statistics to test know a little more to our visitors and how to use our site. At no time personal data may be shared in order to identify it.
                        <br><br>
                        @include('layouts.partials.title')t may collect information about your Internet usage with a cookie file. The cookies are downloaded to your computer automatically and these are stored on the hard disk with information transferred. The data collected by cookies help us improve our website and any of the services offered to you.
                        <br><br>
                        At all times you have the possibility to reject these cookies, this is done by setting options in your browser to refuse all cookies.
                        <br><br>
                        <h4><b>3. Personal Data Storage</b></h4>
                        <br>
                        The stored information helps us at all times to improve and to provide the service you need, these data can be used in the following way:
                        <br><br>
                        a.- The information collected allows us to show the products or services that we as needed.
                        <br>
                        b.- Changes and improvements to the website can justify the use of your information because our goal is to meet your needs.
                        <br>
                        c.- We can offer your contact to third parties as long as you have given your explicit consent.
                        <br>
                        d.- You will at all times we can ask the concealment of data communications with third parties.
                        <br>
                        e.- No information collected by @include('layouts.partials.title') about you will offer identifiable information. We can share information with a third party will only be statistical, at no time disclose their identity.
                        <br><br>
                        <h4><b>4. Information Disclosure</b></h4>
                        <br>
                        Sometimes we may disclose personal information to people working in our group, such as subsidiaries, holding companies or other subsidiaries involved in our business, if applicable. Disclosure to third parties may occur at the following times:
                        <br><br>
                        The sale of any or all of our business to a third party may result in sharing your information.
                        <br>
                        At the time when we are required by law to disclose information about you and your visits to our sites.
                        <br>
                        In order to prevent fraud and help in fraud protection in order to reduce risk, then we may disclose information
                        <br><br>
                        <h4><b>5. Third party links</b></h4>
                        <br>
                        Third-party links have their own privacy policies in which you agree to when you click the link. We at no time we be liable or accept responsibility for third party links. Our liability covers us only on our Site and so we are not responsible for third-party links as we haven’t control over them.
                        <br><br>
                        <h4><b>6. Contact us</b></h4>
                        <br>
                        If you have any questions about this Privacy Policy, please
                        <a href="mailto:info@acortlink.com" style="color:black">
                          contact us
                        </a>
                        <br><br>
                        <b><i>This document was last updated on January 14th, 2018</i></b>
                        <br><br>
                      </div>
                    </td>
                  </tr>
                </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>


  <footer class="footer">
    <div class="container">
      <p>
          <strong> &copy;<a href="{{ url('/') }}">@include('layouts.partials.title')</a></strong> {{ date('Y')}} , All rights reserved.

      </p>
    </div>
  </footer>
  <div id="top" >
    <span class="glyphicon glyphicon-chevron-up"></span>
  </div>
  {{-- @include('layouts.publicidad.happytag') --}}

</body>
</html>
