@extends('layouts.app')
@section('contentheader_title')
    Dashboard
@endsection
@section('htmlheader_title')
    Dashboard
@endsection


@section('content')

    <div class="row">
        <div class=" col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-files-o"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Views this month</span>
                    <span id="viewMonth" class="info-box-number">
                      @if (isset($viewthismonth) && !empty($viewthismonth))
                        {{ $viewthismonth->clicksthismonth}}
                      @else
                        0
                      @endif
                    </span>
                  </div>
            </div>
        </div>
        <div class=" col-xs-12 col-sm-6 col-md-6 col-lg-3 ">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-files-o"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">All Views</span>
                    <span id="allView" class="info-box-number">
                      @if (isset($allearning) && !empty($allearning))
                        {{ $allearning->allclicks }}
                      @else
                        0
                      @endif
                    </span>
                  </div>
            </div>
        </div>
        <div class=" col-xs-12 col-sm-6 col-md-6 col-lg-3 ">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa  fa-usd"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Earnings this Month</span>
                    <span id="earningMonth" class="info-box-number">
                      @if (isset($viewthismonth) && !empty($viewthismonth))
                        {{ $viewthismonth->earning}}
                      @else
                        0
                      @endif
                    </span>
                  </div>
            </div>
        </div>
        <div class=" col-xs-12 col-sm-6 col-md-6 col-lg-3 ">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa  fa-usd"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">All Earnings</span>
                    <span id="allEarning" class="info-box-number">
                      @if (isset($allearning) && !empty($allearning))
                        {{ $allearning->earning }}
                      @else
                        0
                      @endif
                    </span>
                  </div>
            </div>
        </div>

    </div> <!-- END_ROW -->










    <div class="box box-primary">
          <div class="box-header with-border">
            <h4  id="t"  class="text-left"><i class="fa fa-bar-chart-o"></i>
              VIEWS
            </h4>
            {{-- <div class="box-tools pull-right">
              <span class="pull-right">
                <select id="year1" class="btn btn-primary" name="year">
                    <option value=""></option>
                </select>
                <select id="m1" class="btn btn-primary" name="month">
                    <option selected="selected" value=""></option>
                </select>
              </span>
            </div> <!-- end box-tools --> --}}

          </div> <!-- end box -->
          <!-- otro -->
        <div id="chart1" class="box-body  chart-responsive">
            <canvas id="myChart1"  width="600" height="150"></canvas>
        </div>
    </div>

    <!-- EARNING MONTH -->
    <div class="box box-success">
        <div class="box-header with-border">
            <h4 class="text-left"><i class="fa fa-bar-chart-o"></i>
              EARNING THIS MONTH
            </h4>
            {{-- <div class="box-tools pull-right">
              <span class="pull-right">
                <select id="year2" class="btn btn-primary" name="year">
                    <option value=""></option>
                </select>
                <select id="m2" class="btn btn-primary" name="month">
                      <option selected="selected" value=""></option>
                </select>
              </span>
            </div> <!-- end box-tools --> --}}
        </div>
        <div id="chart2" class="box-body chart-responsive"  >
            {{-- {!!$chart_earning->render()!!} --}}
            <canvas id="myChart2"  width="600" height="150"></canvas>
        </div>
    </div> <!-- box succes earnign month -->

    <form class="" action="#" method="post">
      <input type="hidden" name="_token" value="{{csrf_token()}}" id="t">
    </form>
@endsection


@section('scripts')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.js"></script> --}}
<script src="{{ asset('/js/Chart.js') }}"></script>
<script>
$(document).ready(function() {
  // ajaxMyChart1(); // Ganancia en ventas por año
  llenarMyChart1();
  llenarMyChart2();

}); // end ready

// function ajaxMyChart1(){
//     var y = new Date();
//     y = parseInt(y.getFullYear());
//     $('#myChart1_year').empty();
//     for(var i=2019;i<=y;i++){
//         console.log("year: "+i);
//         $('#myChart1_year').append('<option value="'+i+'"> '+i+' </option>');
//     }
//     var year= $('#myChart1_year').val();
//     llenarMyChart1(year);
// }


// char 1
function llenarMyChart1(){
    // var year = $('#myChart1_year').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '{!! route('viewsthismonth') !!}',
        type: 'get',
        dataType: 'json',
        data: {
            // 'year': year,
        },
        headers:{
            "X-CSRF-TOKEN": CSRF_TOKEN,
        } ,
        // data: {param1: 'value1'},
    })
    .done(function(data,success) {
      console.log(data);
        myChart1(data.data.dias, data.data.clicksthismonth);
    })
    .fail(function(e) {
        // console.log(e);
    })
    .always(function() {
        // console.log("complete");
    });
}


function myChart1(dias,clicksthismonth){
    var ctx1 = document.getElementById("myChart1").getContext('2d');
    // Chart.defaults.global.animation.duration = 4000;
    var myChart1 = new Chart(ctx1, {
        type: 'line',
        animation: 4000,
        data: {
            labels: dias,
            datasets: [{
                // label: '# of Votes',
                label: 'Views ',
                data: clicksthismonth,
                backgroundColor: [
                    'rgba(11, 125, 12, 0.5)'
                ],
                borderColor: [
                    'rgba(22, 159, 5, 1)',
                ],
                borderWidth: 2,
                // pointBorderColor: ['rgba(255, 255, 255, 0.1)'],
                lineTension: 0.1,
                // pointBackgroundColor: ['rgba(255, 255, 255, 0.5)'],
                pointRadius: 4,
            }]
        },
        options: {
            legend: {display: false},
            title: {
                display: false,
                text: 'titulo'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}// myChart1


//char 2
function llenarMyChart2(){
    // var year = $('#myChart1_year').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '{!! route('earningthismonth') !!}',
        type: 'get',
        dataType: 'json',
        data: {
            // 'year': year,
        },
        headers:{
            "X-CSRF-TOKEN": CSRF_TOKEN,
        } ,
        // data: {param1: 'value1'},
    })
    .done(function(data,success) {
      console.log(data);
        myChart2(data.data.dias, data.data.earning);
    })
    .fail(function(e) {
        // console.log(e);
    })
    .always(function() {
        // console.log("complete");
    });
}




function myChart2(dias,earning){
    var ctx2 = document.getElementById("myChart2").getContext('2d');
    // Chart.defaults.global.animation.duration = 4000;
    var myChart2 = new Chart(ctx2, {
        type: 'line',
        animation: 4000,
        data: {
            labels: dias,
            datasets: [{
                // label: '# of Votes',
                label: 'Earnings ',
                data: earning,
                backgroundColor: [
                    'rgba(6, 13, 99, 0.5)'
                ],
                borderColor: [
                    'rgba(14, 90, 179, 0.87)',
                ],
                borderWidth: 2,
                // pointBorderColor: ['rgba(255, 255, 255, 0.1)'],
                lineTension: 0,
                // pointBackgroundColor: ['rgba(255, 255, 255, 0.5)'],
                pointRadius: 4,
            }]
        },
        options: {
            legend: {display: false},
            title: {
                display: false,
                text: 'titulo'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}// myChart2
</script>

@endsection
