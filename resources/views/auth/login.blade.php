@extends('layouts.auth')
@section('content')

    @if (!empty(session('notification')))
      <div class="text-center">
        <div class="alert alert-success">
          {{session('notification')}}
        </div>
      </div>
    @endif

    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <form method="POST" action="{{ route('login') }}">
          @csrf
        <div class="form-group has-feedback">
          <input name="email" type="email"  class="form-control "   placeholder="Email" value="{{ old('email') }}" required>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          {{-- @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror --}}
        </div>
        <div class="form-group has-feedback">
          <input name="password" type="password" class="form-control " placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          {{-- @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror --}}
        </div>

        {{-- captcha --}}
        <div class="" style="padding: 30px 5px 30px 0px;">
          <div >
            <div id="captcha"></div>
          </div>
          <div class="g-recaptcha" data-sitekey="6LcRPSsUAAAAAGJtsUISh4ykH-UQridxQkZdTt8R"></div>
        </div>


        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox"> Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      {{-- <div class="social-auth-links text-center">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
          Facebook</a>
        <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
          Google+</a>
      </div> --}}
      <!-- /.social-auth-links -->

      <a href="{{route('password.request')}}">I forgot my password</a><br>
      <a href="{{route('register')}}" class="text-center">Register a new membership</a>

    </div>
    <!-- /.login-box-body -->

    <div class="text-center">
      @include('layouts.publicidad.anuncio_in-feed_solotexto')
    </div>


@endsection
