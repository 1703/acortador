@extends('layouts.auth')

@section('content')

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form method="POST" action="{{ route('register') }}">
        @csrf

        @if (isset($codereferral))
          <input type="text" hidden="hidden" name="codereferral" value="{{ $codereferral }}">
        @endif
        {{-- name --}}
      <div class="form-group has-feedback">
        <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Full name" value="{{ old('name') }}" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        {{-- @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror --}}
      </div>
      {{-- email --}}
      <div class="form-group has-feedback">
        <input name="email" type="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        {{-- @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror --}}
      </div>
      {{-- password --}}
      <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        {{-- @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror --}}
      </div>
      {{-- remember password --}}
      <div class="form-group has-feedback">
        <input  name="password_confirmation" type="password" class="form-control" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>



      <div class="" style="padding: 30px 5px 30px 0px;">
        <div >
          <div id="captcha"></div>
        </div>
        <div class="g-recaptcha" data-sitekey="6LcRPSsUAAAAAGJtsUISh4ykH-UQridxQkZdTt8R"></div>
      </div>



      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="terms"> I agree to the <a href="{{url('/terms')}}">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-success btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    {{-- <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div> --}}

    <a href="{{route('login')}}" class="text-center">I already have a membership</a>
  </div>


  <div class="text-center">
    @include('layouts.publicidad.anuncio_in-feed_solotexto')
    
  </div>
@endsection
