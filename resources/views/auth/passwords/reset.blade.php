


@extends('layouts.auth')
@section('content')

    @if (!empty(session('notification')))
      <div class="text-center">
        <div class="alert alert-success">
          {{session('notification')}}
        </div>
      </div>
    @endif


    <div class="login-box-body">
        {{-- <p class="login-box-msg">We will send you a password recovery email.</p> --}}
        <div class="login-box-msg">{{ __('Reset Password') }}</div>

        <form method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group has-feedback">
                <label for="email"  >{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group  has-feedback">
                <label for="password"  ">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group  has-feedback">
                <label for="password-confirm" >{{ __('Confirm Password') }}</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-md-6 text-center">
                    <button type="submit" class="btn btn-success btn-block btn-flat">
                        {{ __('Reset Password') }}
                    </button>
                </div>
            </div>
        </form>

    </div>

    <div class="text-center">
      @include('layouts.publicidad.anuncio_in-feed_solotexto')
    </div>


@endsection