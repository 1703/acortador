

@extends('layouts.auth')
@section('content')

    @if (!empty(session('notification')))
      <div class="text-center">
        <div class="alert alert-success">
          {{session('notification')}}
        </div>
      </div>
    @endif

    @if (!empty(session('status')))
      <div class="text-center">
        <div class="alert alert-success">
          {{session('status')}}
        </div>
      </div>
    @endif

    
    <div class="login-box-body">
      <p class="login-box-msg">We will send you a password recovery email.</p>
      <form method="POST" action="{{ route('password.email') }}">
          @csrf
        <div class="form-group has-feedback">
          <input name="email" type="email"  class="form-control "   placeholder="Email" value="{{ old('email') }}" required>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          {{-- @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror --}}
        </div>

        <div class="row">
          <div class="col-xs-4">
            <button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
          </div>
        </div>
      </form>


      <a href="{{route('login')}}" class="text-center">Back to sign in</a>

    </div>
    <!-- /.login-box-body -->

    <div class="text-center">
      @include('layouts.publicidad.anuncio_in-feed_solotexto')
    </div>


@endsection