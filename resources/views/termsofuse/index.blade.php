<!DOCTYPE html>
<html lang="en">
@include('layouts.partials.htmlheader')
<body >

  <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
          <div class="navbar-header">
              <a class="navbar-brand" href="{{url('/')}}"><b>@include('layouts.partials.title')</b></a>
          </div>
        </div>
  </div>




<div id="rows" >
  <div class="container">
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 centered">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title text-center"><strong>TERMS OF USE</strong></h3>
          </div>
          <div class="panel-body">
            <table>
              <tbody>
                <tr>
                  <td>
                    <div align="justify">
                      <br>
                      The following terms and conditions guide all use of the website @include('layouts.partials.title') and all content, services and products on this website are offered, including the customer area.
                      <br><br>
                      This website belongs to @include('layouts.partials.title') is subject to all terms and conditions contained in this document and all other operating rules as are privacy policies.
                      <br><br>
                      Before accessing or using the site carefully read this agreement 'terms and conditions' because when accessing @include('layouts.partials.title') you agree to be bound by the terms and conditions of this agreement. If you disagree with the terms and conditions, then you can’t access the Site or use any of the services offered by @include('layouts.partials.title').
                      <br><br>
                      <h4><b>Description of services</b></h4>
                      <br>
                      Our site offers the possibility of shortening supports URLs and of this form earn revenue by advertising that is displayed to users on their way to the destination URL.
                      <br><br>
                      Advertisers can use our site to promote their products or services in advertising pages provided by the media or any third party. Therefore you must understand and accept that AcrotLink may include advertisements in these shortened URLs because it is a requirement to operate on the page.
                      <br><br>
                      <h4><b>Client Area</b></h4>
                      <br>
                      You at all times is responsible for maintaining the security of customer area and therefore is also responsible for all activities that occur under your account and any other action taken in relation to your account.
                      <br><br>
                      If you identify unauthorized use on your account or any other security violation must immediately inform @include('layouts.partials.title')
                      <br><br>
                      @include('layouts.partials.title') not be liable for the acts or omissions not reported by you including any damage they may cause.
                      <br><br>
                      <h4><b>Contribution to the Website</b></h4>
                      <br>
                      If you leave comments, published materials, publishes links anywhere on the site (or allows a third party to do-what in the name) you will be solely responsible for the content displayed and the damage this can cause, this content can be text, graphics, audio or computer software.
                      <br><br>
                      By making public the content available you must ensure at all times that:
                      <br><br>
                      • You meet all licenses relating to the content you offer and has done everything necessary to pass this information to end users.
                      <br>
                      • The content you offer doesn’t contain at no time to install any viruses, malware, worms, Trojan or other content that may be harmful or destructive.
                      <br>
                      • Copy, download and use the content may not violate the property rights, including copyrights, patents, trademarks, copyrights, trade secrets or any third party.
                      <br>
                      • The content may not be libelous or defamatory of course can’t contain threats or incite violence against persons or entities and shall not violate the privacy rights of third parties.
                      <br>
                      • You must ensure that the content isn’t spam and doesn’t contain unethical trade issues. Nor can mislead recipients as to the source of the material (such as identity theft).
                      <br><br>
                      @include('layouts.partials.title') at all times have the right to remove or reject any content that in the opinion of @include('layouts.partials.title') violates any privacy or harm the user. Likewise @include('layouts.partials.title') has the power to deny access and use the site to any person or entity for any reason.
                      <br><br>
                      @include('layouts.partials.title') shall haven’t obligation to repay the amounts generated in the case that the aforementioned infringements are given.
                      <br><br>
                      <h4><b>Advertiser Restrictions</b></h4>
                      <br>
                      @include('layouts.partials.title') can sanction invalid or fraudulent clicks, these can include (but not limited) clicks generated by:
                      <br><br>
                      • The publication intermediate popup ads.
                      <br>
                      • Obtaining impressions by paying directly or indirectly to others for visits to link.
                      <br>
                      • Not supplant referrals, referrals send blank, or falsify referrals. All traffic sources must be verifiable through referrals.
                      <br>
                      • The generation of views with use of robots, proxies, auto-refreshes or other methods.
                      <br>
                      • The distribution of illegal content or child pornography.
                      <br>
                      • You can’t send users to our URL shortener involuntarily outside the automatic script.
                      <br>
                      • Hacking other websites illegally to display their links.
                      <br>
                      • Correctly categorize the content of your link as adult or not adult.
                      <br>
                      • Spamming Junk other sites without permission, using their links.
                      <br>
                      @include('layouts.partials.title') reserves the full right to put your account in a state of limited functionality if the account performance is very poor. Limited account status means that the user can’t longer generate income or removed from the site. Poor performance is usually caused by fraudulent clicks.
                      <br><br>
                      <h4><b>Adult content</b></h4>
                      <br>
                      @include('layouts.partials.title') not accept adult content sites (pornography), so any link that has been shortened to a web adult content may be canceled by the site. If the user repeats the infraction @include('layouts.partials.title') reserves the right to delete the user account, @include('layouts.partials.title') not pay any income from adult content pages.
                      <br><br>
                      <h4><b>Publisher payments</b></h4>
                      <br>
                      @include('layouts.partials.title') make payments via PayPal, when the publisher has reached a minimum payment of $ @include('layouts.partials.usd') (provided that the publisher has indicated its data in your account for the payment). Payments will be made on 1st and 16th of each month, but payments can take 7 to 15 days if these payments have been marked by @include('layouts.partials.title') as supports to review, in order to combat fraud that may indicate bad faith by the publisher.
                      <br><br>
                      <h4><b>Disclaimer of warranties</b></h4>
                      <br>
                      @include('layouts.partials.title'), its supports and its advertisers disclaim all warranties of any kind, express or implied, including but not limited to, warranties of merchantability.
                      <br><br>
                      Neither @include('layouts.partials.title') nor its supports and advertisers ensure that the Site is error-free or that access thereto will be continuous or uninterrupted.
                      <br><br>
                      You must understand that it is your responsibility to ensure that privacy policies are adapted to the needs of your company or soporte.
                      <br><br>
                      @include('layouts.partials.title') is not responsible for the privacy policies of the media who use our services and we make no representation or warranty, express or implied, that the privacy policies created by the supports who use our service are complete, accurate or free errors or omissions.
                      <br><br>
                      <h4><b>Limitation of Liability</b></h4>
                      <br>
                      @include('layouts.partials.title'), its advertisers or its supports shall not be liable in any way on the points addressed in this agreement under any contract, negligence, strict liability or any other legal or equitable category for:
                      <br><br>
                      • The interruption of use or loss or corruption of data.
                      <br>
                      • Any special, incidental or consequential damages.
                      <br>
                      • For any amount exceeding the fees paid by you to @include('layouts.partials.title') under this agreement.
                      <br>
                      @include('layouts.partials.title') not assume any liability for any failure or delay owing to matters beyond its reasonable control. The site will not be liable for any special or consequential damages resulting from the use or inability to use the services and products offered on this site, or presentation of services and products.
                      <br><br>
                      <h4><b>Representation and Warranty</b></h4>
                      <br>
                      You must represent and warrant that:
                      <br><br>
                      • Your use of the Site shall be in strict accordance with the Privacy Policy @include('layouts.partials.title') with this agreement and all applicable laws and regulations.
                      <br>
                      • Your use of the site will not be to infringe or misappropriate form of intellectual property of others.
                      <br><br>
                      <h4><b>Changes</b></h4>
                      <br>
                      @include('layouts.partials.title') reserves the full right to modify or replace any part of this agreement, it is your responsibility to periodically review the terms and conditions of the site. Modification or replacement of such terms in this agreement constitute acceptance of such changes by users, supports and advertisers.
                      <br><br>
                      @include('layouts.partials.title') may in the future offer new services and / or characteristics through the Site, these new features and / or services will also be subject to the terms and conditions of this agreement.
                      <br><br>
                      <h4><b>Termination</b></h4>
                      <br>
                      @include('layouts.partials.title') has the right to terminate your access to all the Site at any time, with or without cause, with or without notice, with immediate effect.
                      <br><br>
                      If you want to terminate this Agreement you can simply stop using the Site.
                      <br><br>
                      <h4><b>Compensation</b></h4>
                      <br>
                      You agree to indemnify and hold harmless at all times @include('layouts.partials.title'), their supports, advertisers, directors, employees and account managers against any claims and expenses, including attorneys' fees this, the inconveniences arising from use in the site, including but not limited to your violation on this site.
                      <br><br>
                      <h4><b>Changes in our terms and privacy policies</b></h4>
                      <br>
                      @include('layouts.partials.title') may make occasional adjustments to our privacy policies and terms of service, the user must be coming to stay informed of these changes. The use of the Site after changes constitutes acceptance of those changes.
                      <br><br>
                      <h4><b>Contact us</b></h4>
                      <br>
                      If you have any questions about the terms and conditions of use, please
                        <a href="mailto:info@acortlink.com" style="color:black">
                          contact us
                        </a>
                      <br><br>
                      <b><i>This document was last updated on January 14th, 2019</i></b>
                      <br><br>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>




<div id="top" >
  <span class="glyphicon glyphicon-chevron-up"></span>
</div>

  <footer class="footer">
    <div class="container">
      <p>
          <strong> &copy;<a href="{{ url('/') }}">@include('layouts.partials.title')</a></strong> {{ date('Y')}} , All rights reserved.
      </p>
    </div>
  </footer>


</body>
</html>
