<?php


// Route::get('/', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('logout','Auth\LoginController@logout');

// ruta auth
Route::middleware(['auth'])->group(function(){
  Route::get('/home', 'HomeController@index')->name('home');
  // links
  Route::get('link/create','LinkController@create')->name('link.create');
  Route::get('link','LinkController@index')->name('link.index');
  Route::put('link/update/{link}','LinkController@update')->name('link.update');
  Route::get('link/edit/{link}','LinkController@edit')->name('link.edit');
  Route::post('link/store','LinkController@store')->name('link.store');
  Route::get('link/destroy/{link}','LinkController@destroy')->name('link.destroy');

  // quicklink view
  // Route::get('quicklink','QuicklinkController@index')->name('quicklink.index');

  // api view
  Route::get('api','ApiController@index')->name('api.index');

  // users
  Route::get('users','UsersController@index')->name('users.index')->middleware('admin');

  //referrals
  Route::get('referral','ReferralController@index')->name('referral.index');

  // settings
  Route::get('settings','SettingsController@index')->name('settings.index');
  Route::post('settings/update','SettingsController@update')->name('settings.update');

  // payments
  Route::get('payments','PagoController@index')->name('payments.index');

  // charts
  Route::get('viewsthismonth','HomeController@viewsthismonth')->name('viewsthismonth');
  Route::get('earningthismonth','HomeController@earningthismonth')->name('earningthismonth');

  // admin
  // Route::get()->name('admin.');
});
// end rutas auth
// reset passsword
Route::post('store','LinkController@stor')->name('link.stor');

Route::get('terms','TermsOfUseController@index')->name('terms.index');
Route::get('privacy','PrivacyPolicyController@index')->name('privacy.index');

Route::get('register/verify/{code}','Auth\RegisterController@verify');

Route::get('ref/{codereferral}','ReferralController@ref')->name('referral.ref');
Route::post('validate','SearchController@captchaForm'); //valida
Route::post('captcha','SearchController@captchaForm');
Route::post('ok','SearchController@ok');
Route::get('{code}','SearchController@index')->where(['code'=>'[a-z-A-Z-0-9]+']);
