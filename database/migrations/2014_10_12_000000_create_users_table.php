<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('idcode',9)->collation('latin1_bin')->nullable(); //codificacion sencible a mayusculas y minusculas
          $table->string('name')->nullable();
          $table->string('lastname')->nullable();
          $table->string('email')->unique();
          $table->string('password')->nullable();
          $table->string('direccion')->nullable();
          $table->string('codepostal')->nullable();
          $table->string('ciudad')->nullable();
          $table->string('pais_id')->nullable();
          $table->boolean('activo')->default(false);
          $table->string('confirmation_code')->nullable();
          $table->boolean('isadmin')->default(false);
          $table->integer('metodopago_id')->default(1); // paypal
          $table->index('idcode','index_users_idcode');
          $table->rememberToken();
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
