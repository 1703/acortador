<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLinksInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links_info', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->double('valor',15,5);
          $table->boolean('check_referrall')->default(false); //*
          $table->boolean('verificado')->default(false); // **
          $table->string('remote_addr')->default(false);
          $table->boolean('paystate')->default(false); // ***
          $table->bigInteger('idlink')->unsigned();
          $table->string('periodo',7)->default(0);
          $table->date('fecha')->default(null);

          $table->string('hostname')->default(false);
          $table->string('uname')->default(false);//"Linux franco 4.4.0-89-generic #112~14.04.1-Ubuntu SMP Tue Aug 1 22:08:32 UTC 2017 x86_64"
          $table->string('isp')->default(false);
          $table->string('regionname')->default(false); //tucumn
          $table->string('city')->default(false);//san miguel de tucuman
          $table->string('countrycode')->default(false); //ar me co
          $table->string('countryname')->default(false); //pais
          $table->string('codigopostal')->default(false);//4000
          $table->string('continent')->default(false);
          $table->string('port',10)->default(false);
          $table->string('latitud')->default(false);
          $table->string('longitud')->default(false);
          $table->string('timezone')->default(false);
          $table->string('codigoarea')->default(false);//381 3865
          $table->enum('isproxy',['no_verificado','si','no'])->default('no_verificado');
          $table->string('anonimousproxy')->default(false);
          $table->string('tipoproxy')->default(false);
          $table->string('domainname')->default(false); //telecom.com.ar
          $table->string('useragent')->default(false);

          //index
          $table->index('remote_addr','index_linkinfo_remote_addr');
          // $table->foreign('idlink')->references('id')->on('link');
          $table->foreign('idlink')->references('id')->on('link')->onSoftDeletes('cascade');
          $table->timestamps();
          $table->softDeletes();


          // *
          /* check_referrall: cambia a 1 cuando chequeo si un usuario tiene referido,
             si tiene referido cuento cada linkinfo sumando su valor y cambio el estado
            de linkinfo.check_referrall a 1 (para saver que ya fue contado y no contarlo de nuevo.)
          */

          // **
          /*  verificado: si el usuario tiene dos accesos desde la misma ip tendra dos linkinfo con estado verificado = 1
              los demas veficado tendran valor 0
          */
          // ***
          /*  paystate: cuando llegue la fecha de pago (pago.fechapago) cuento todos los linkinfo.paystate y les asigno 1 para saber que ya fueron contados para pago.
              tendran valor 0 cuando no se allan contado para pago.
          */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links_info');
    }
}
