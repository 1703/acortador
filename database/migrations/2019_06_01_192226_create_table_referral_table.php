<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReferralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->bigInteger('iduser')->unsigned();
          $table->string('codereferral')->nullable(); //codigo del usuario
          // $table->string('porcentaje'); //20%
          // $table->string('ganancia');

          //index
          $table->index('codereferral','index_referral_codereferral');
          $table->foreign('iduser')->references('id')->on('users');
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral');
    }
}
