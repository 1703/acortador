<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('code')->collation('latin1_bin'); //codificacion sensible a mayusculas y minusculas
          $table->string('url');
          $table->index('code','index_link_code');

          $table->bigInteger('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
