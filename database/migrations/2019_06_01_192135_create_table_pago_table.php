<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pago', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->date('fechapago_inicio'); // fecha de primer pago
          $table->date('fechapago_fin'); // la siguiente fecha de pago
          // $table->enum('tipo',['mensual','quincenal'])->default('mensual');
          $table->enum('estado',['creado','pendiente','noaprobado','pagado'])->default('creado'); // 0,1,2
          $table->double('monto',15,5)->default('0.0');

          $table->integer('metodopago_id')->unsigned()->nullable();
          $table->foreign('metodopago_id')->references('id')->on('metodopago');

          $table->bigInteger('email_id')->unsigned()->nullable();
          $table->foreign('email_id')->references('id')->on('users_email');

          $table->bigInteger('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');

          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pago');
    }
}
