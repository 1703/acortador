<?php

use Illuminate\Database\Seeder;
use App\Pago;
use Carbon\Carbon;

class PagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker =Faker\Factory::create();
        // Pago::create([
        //   'fechapago_inicio'=> $faker->dateTimeBetween($startDate = '-2 month', $endDate = 'now'),
        //   'fechapago_fin'=>  $faker->dateTimeBetween($startDate = '-2 month', $endDate = 'now'),
        //   'estado'=>1,
        //   'monto'=>0,
        //   'metodopago_id'=>1,
        //   'email_id'=>1,
        //   'user_id'=>1
        // ]);
        $fechaActual= Carbon::now()->setDate(date('Y'), date('m'), date('d'))->toDateString();
        $fechaPrimerPago = Carbon::now()->setDate(date('Y'), date('m')+1, date('d'))->toDateString();
        Pago::create([
          'fechapago_inicio'=>$fechaActual,
          'fechapago_fin' => $fechaPrimerPago,
          'monto'=> '0.00000',
          'metodopago_id'=>1,
          'user_id'=>2,
          'email_id'=>2
        ]);
        Pago::create([
          'fechapago_inicio'=> $fechaActual,
          'fechapago_fin' => $fechaPrimerPago,
          'monto'=> '0.00000',
          'metodopago_id'=>1,
          'user_id'=>1,
          'email_id'=>1
        ]);
    }
}
