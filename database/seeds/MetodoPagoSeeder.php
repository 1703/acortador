<?php

use Illuminate\Database\Seeder;
use App\Metodopago;
class MetodoPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Metodopago::create([
          'metodopago'=>'paypal',
        ]);
    }
}
