<?php

use App\Link;
use App\User;
use App\Email;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // $faker =Faker\Factory::create();

      $user = User::create([
        'idcode'=> $this->ran(),
        'name'=>'administrador',
        'email'=>'administrador@gmail.com',
        'password'=> bcrypt('francos@1703'),
        'activo' => true,
        'isadmin'=> true,
        'metodopago_id'=>1
      ]);

      Email::create([
        'email'=> $user->email,
        'users_id'=> $user->id,
      ]);

      // direct url 
      // http://deloplen.com/afu.php?zoneid=2711931
      // http://deloplen.com/afu.php?zoneid=2708974
      // http://deloplen.com/afu.php?zoneid=2711934
      Link::create([
        'code'=>$this->ran(),
        'url' => 'http://deloplen.com/afu.php?zoneid=2711931',
        'user_id' =>$user->id,
      ]);

      Link::create([
        'code'=>$this->ran(),
        'url' => 'http://deloplen.com/afu.php?zoneid=2708974',
        'user_id' =>$user->id,
      ]);
      Link::create([
        'code'=>$this->ran(),
        'url' => 'http://deloplen.com/afu.php?zoneid=2711934',
        'user_id' =>$user->id,
      ]);

      
      $user = User::create([
        'idcode'=> $this->ran(),
        'name'=>'franco',
        'email'=>'franco170391@gmail.com',
        'password'=> bcrypt('francos@'),
        'activo' => true,
        'metodopago_id'=>1
      ]);
      Email::create([
        'email'=> $user->email,
        'users_id'=> $user->id,
      ]);
    }

    public function ran(){
        $an = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $su = strlen($an) - 1;
        return $abc=
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1);
    }
}
