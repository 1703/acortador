<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Email;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $faker = Faker\Factory::create();

      for ($i=0; $i <5 ; $i++) {
        $user = User::create([
          'idcode'=> $this->ran(),
          'name'=>$faker->name,
          'email'=>$faker->email,
          'password'=> bcrypt('francos'),
          'activo' => true,
          'metodopago_id'=>1
        ]);
        // email
        Email::create([
          'email'=> $user->email,
          'users_id'=> $user->id,
        ]);
      }



    }

    public function ran(){
        $an = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $su = strlen($an) - 1;
        return $abc=
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1);
    }
}
