<?php

use Illuminate\Database\Seeder;
use App\Email;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker =Faker\Factory::create();

        Email::create([
          'email'=> $faker->email,
          'users_id'=>1
        ]);
    }
}
