<?php

use Illuminate\Database\Seeder;
use App\User;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker =Faker\Factory::create();
        for ($i=1; $i < 3; $i++) {
            $u= User::findOrFail($i);
            for ($i=1; $i < 10; $i++) {
              // code...
              $created_at= $faker->dateTimeBetween($startDate = '-2 month', $endDate = 'now');
              $idlink=\DB::table('link')->insertGetId([
                  // 'id'=>$j,
                  'code'=>$this->ran(),
                  'url'=>$faker->url,
                  'user_id'=>$u->id,
                  'created_at'=>$created_at
                ]);
            }// end for
            // $total= rand(50,100);
            // echo "total: ".$total."\n";
            // for ($ii = 0; $ii < $total; $ii++) {
            //     DB::table('linkinfo')->insert([
            //     'valor'=>$this->f_rand(),
            //     'verificado'=>1,
            //     'paystate'=>0,
            //     'idlink'=>$idlink,
            //     'remote_addr'=>$faker->ipv4,
            //     'useragent'=>$faker->userAgent,
            //     'created_at'=>$created_at
            //   ]);
            // }
        }
    }




    public function ran(){
        $an = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $su = strlen($an) - 1;
        return $abc=
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1) .
            substr($an, rand(0, $su), 1);
    }


  	public function f_rand(){
  	    //de 0,80 a 1,50 USD
  	    $min=0.00070;
  	    $max=0.00150;
  	    $mul=10000;
  	    if ($min>$max) return false;
  	    return mt_rand($min*$mul,$max*$mul)/$mul;
  	}


}
