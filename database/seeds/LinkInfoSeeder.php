<?php

use Illuminate\Database\Seeder;
use App\LinkInfo;

class LinkInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker =Faker\Factory::create();
        for ($i=0; $i <2000 ; $i++) {
          $created_at= $faker->dateTimeBetween($startDate = '-1 month', $endDate = 'now');
          LinkInfo::create([
            'valor'=> $this->f_rand(),
            'idlink'=> $faker->numberBetween(1,10),
            'remote_addr'=> $faker->ipv4,
            'periodo'=> date_format($created_at, 'Y-m'),
            'created_at'=>$created_at
          ]);
        }

    }


    public  function f_rand(){
      //   1.5 USD para cualquier pais de donde provenga el acceso
      $min=0.00070;
      $max=0.0015;
      $mul=10000;
        if ($min>$max) return false;
        return mt_rand($min*$mul,$max*$mul)/$mul;
    }
}
