<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(MetodoPagoSeeder::class);
      $this->call(PaisesSeeder::class);
      $this->call(UserAdminSeeder::class);
      // $this->call(UsersSeeder::class);
      // $this->call(LinkSeeder::class);
      // $this->call(EmailSeeder::class);
      $this->call(PagoSeeder::class);
    }
}
