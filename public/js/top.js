$(document).ready(function(){
  $('#top').click(function(){
    $('body, html').animate({
      scrollTop: '0px'
    }, 300);
  });

  $(window).scroll(function(){
    if( $(this).scrollTop() > 0 ){
      $('#top').css({
        display: 'inline-block'
      });
      $('#top').slideDown(300);
    } else {
      $('#top').css({
        display: 'inline-block'
      });
      $('#top').slideUp(300);
    }
  });

});
