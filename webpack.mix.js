const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.copyDirectory('bower_components/bootstrap/fonts', 'public/fonts');
mix.copyDirectory('bower_components/font-awesome/fonts', 'public/fonts');
mix.copyDirectory('bower_components/Ionicons/fonts', 'public/fonts');
mix.copyDirectory('node_modules/admin-lte/dist/img', 'public/img');
mix.copyDirectory('node_modules/admin-lte/plugins', 'public/plugins');

mix
.styles([
  'bower_components/bootstrap/dist/css/bootstrap.min.css',
  'bower_components/font-awesome/css/font-awesome.min.css',
  'bower_components/Ionicons/css/ionicons.min.css',
  'node_modules/admin-lte/dist/css/AdminLTE.min.css',
  'node_modules/admin-lte/dist/css/skins/_all-skins.min.css',
  // 'node_modules/admin-lte/plugins/iCheck/square/blue.css',
  // 'node_modules/admin-lte/plugins/iCheck/square/_all.css',
  'bower_components/morris.js/morris.css',
  'bower_components/jvectormap/jquery-jvectormap.css',
  'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
  'bower_components/bootstrap-daterangepicker/daterangepicker.css',
  'public/css/ocultar.texto.menu.css',
],'public/css/app.css')


.styles([
  'public/css/main.css'
],'public/css/main.min.css')


.styles([
  'public/css/top.css'
],'public/css/top.min.css')

.styles([
  'public/css/landing.css'
],'public/css/landing.min.css')

.styles([
  'public/css/termsofuse.css'
],'public/css/termsofuse.min.css')


.styles([
  'bower_components/bootstrap/dist/css/bootstrap.min.css',
  'bower_components/font-awesome/css/font-awesome.min.css',
  'bower_components/Ionicons/css/ionicons.min.css',
  'node_modules/admin-lte/dist/css/AdminLTE.min.css'
],'public/css/auth.css')


.scripts([
  'bower_components/jquery/dist/jquery.min.js',
  'bower_components/jquery-ui/jquery-ui.min.js',
  'bower_components/bootstrap/dist/js/bootstrap.min.js',
  //Morris
  'bower_components/raphael/raphael.min.js',
  'bower_components/morris.js/morris.min.js',
  //Sparkline
  'bower_components/jquery-sparkline/dist/jquery.sparkline.min.js',
  //jvectormap
  'bower_components/jquery-knob/dist/jquery.knob.min.js',
  'bower_components/moment/min/moment.min.js',
  'bower_components/bootstrap-daterangepicker/daterangepicker.js',
  'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
  //Slimscroll
  'bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
  //FastClick
  'bower_components/fastclick/lib/fastclick.js',
  'node_modules/admin-lte/dist/js/adminlte.min.js'
  // 'node_modules/admin-lte/plugins/iCheck/icheck.min.js',
  // 'node_modules/admin-lte/dist/js/demo.js',
],'public/js/app.js')


.scripts([
  'bower_components/jquery/dist/jquery.min.js',
  'bower_components/bootstrap/dist/js/bootstrap.min.js'
],'public/js/auth.js');
